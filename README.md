# Lancer environnement docker

--- 

## Prerequis

Avoir docker installé avec docker-compose

---

## Pour lancer l'environnement via ligne de commande:

- se situer dans le meme dossier que le fichier [docker-compose.yml](docker-compose.yml)
- ouvrir un terminal
- Lancer la commande `docker-compose up`

(optionnel) Pour lancer la commande en background ajouter ` -d` pour detach

Pour Stopper un service container lancé en background, simplement faire docker-compose stop

## Pour lancer l'environnement depuis les scripts:

- `./start.sh` pour copier le fichier .env.example dans .env s'il n'existe pas et lancer les containers

- `./stop.sh` pour stopper le container

- `./delete.sh` pour Arrête les conteneurs et supprime les conteneurs, les réseaux, les volumes et les images créés par up
, cette commande ne supprime cependant pas les volumes persistants dont le contenu de la base de donnée, le dossier src et les fichiers php.ini et site.conf

- `./delete-restart.sh` pour supprimer tous les documents (pdfs), le volume de la BDD et refaire un clean start des containers

# Développement

Le code php doit se situer dans le dossier --> src/

Pour acceder au site deux possibilités:

Définir un nom de domaine et l'ajouter dans le fichier [/src/config/environment.php](/src/config/environment.php)
L'ajouter ensuite dans [sites.conf](nginx/site.conf) pour la clé server_name

exemple pour stage.com
- Ajouter `127.0.0.1 stage.com` dans le fichier `C:\Windows\System32\drivers\etc\host` (attention il faut ouvrir l'éditeur de texte en tant qu'administrateur)
- Ajouter `stage.com` dans  [/src/config/environment.php](/src/config/environment.php)
- Executer la commande `./delete-restart.sh le site est disponible sur stage.com, sinon il sera dans tous les cas disponible sur [localhost](http://localhost)
  
## Phpmyadmin

Pour accéder à la base de donnée et débugger, aller à l'addresse [127.0.0.1:8081](http://127.0.0.1:8081)
et se connecter avec les identifiants présents dans PMA_USER et PMA_PASSWORD dans le fichier [docker-compose.yml](docker-compose.yml)
