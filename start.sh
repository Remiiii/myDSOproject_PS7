#!/bin/sh

[ ! -f .env ] && cp .env.example .env

docker-compose up -d &&
  echo "Services running on host 127.0.0.1 or stage.com" &&
  echo "PHPMyadmin is running on address 127.0.0.1:8081 or stage.com:8081"
