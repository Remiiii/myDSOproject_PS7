<?php

namespace App\Middlewares;

require_once 'middlewares/BaseMiddleware.php';
require_once 'models/UserModel.php';

class NeedLogged extends BaseMiddleware
{

    /**
     * A modifier pour vérifier que l'utilisateur est connecté
     * @param $request
     * @throws \Exception
     */
    function handle($request)
    {
        return $this->isLogged();
    }
}