<?php


namespace App\Middlewares;


interface Middleware
{

    /**
     * retourne true si la condition est respecté sinon false
     * @param $request
     * @return mixed
     */
    public function handle($request);
}