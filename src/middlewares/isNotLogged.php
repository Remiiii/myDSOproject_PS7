<?php

namespace App\Middlewares;

require_once 'middlewares/BaseMiddleware.php';
/**
 * Si un utilisateur connecté veut retourner a la page login sans s'être déconnecté
 */
class isNotLogged extends BaseMiddleware
{

    public function handle($request)
    {
        return !$this->isLogged();
    }
}