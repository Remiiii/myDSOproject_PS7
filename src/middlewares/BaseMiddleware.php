<?php

namespace App\Middlewares;
use App\Controllers\ApplicationController;

require_once 'middlewares/Middleware.php';

abstract class BaseMiddleware implements Middleware
{

   public static function redirect($path){
       if(ApplicationController::getInstance()->keyExists($path)){
           header('Location: '.HTTP_URL.$path);
           exit();
       }
   }

   protected function isLogged(){
       return !empty($_SESSION['user']);
   }

   protected function checkRequiredFieldsExists(array $fields,array $request){
       $intersect=array_intersect($fields,array_keys($request));
       return sizeof($fields)===sizeof($intersect);
   }
}