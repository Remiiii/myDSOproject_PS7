<?php

namespace App\Middlewares;

use App\Models\UserModel;

require_once "middlewares/BaseMiddleware.php";
require_once 'models/UserModel.php';
class NeedAdmin extends BaseMiddleware
{

    public function handle($request)
    {
        $usermodel=new UserModel();

        if($this->isLogged()){
            return $usermodel->checkType("administrateur");
        }
        return false;

    }
}