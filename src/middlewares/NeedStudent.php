<?php

namespace App\Middlewares;
require_once 'middlewares/BaseMiddleware.php';

require_once 'models/UserModel.php';
class NeedStudent extends BaseMiddleware
{
    public function handle($request)
    {
        if($this->isLogged()){
            return (bool)$_SESSION['user']->etudiant;
        }
        return false;
    }
}