<?php

namespace App\Controllers;

class ApplicationController
{

    private static $instance;
    private $routes;

    public static string $DEFAULT_HOME = "home";

    private function __construct()
    {
        // Sets the controllers and the views of the application.
        $this->routes =
            [
                //midlleware -> controller -> view
                self::$DEFAULT_HOME => [
                    'view' => 'homeView',
                    'middlewares' => null
                ],
                'login_form' => [
                    'view' => 'loginForm',
                    'middlewares' => ["isNotLogged"]
                ],
                'error' => [
                    'view' => 'ErrorView',
                    'middlewares' => null
                ],
                'create_account_form' => [
                        'view' => 'createAccountForm',
                        'middlewares' => ["isNotLogged"]
                    ],
                'dashboard' => [
                    'view' => 'dashboardView',
                    'middlewares' => ["NeedLogged"]
                ],
                'admin_users' => [
                    'view' => 'adminUtilisateurView',
                    'middlewares' => ["NeedAdmin"]
                ],
                'planning' => [
                    'view' => 'planningView',
                    'middlewares' => ["NeedAdmin"]
                ],
                'cvtheque' => [
                    'view' => 'cvthequeView',
                    'middlewares' => ["NeedEntreprise","NeedAdmin"]
                ],
                'liste-etud' => [
                    'view' => 'listeEtudView',
                    'middlewares' => ["NeedEntreprise","NeedAdmin"]
                ],
                'offres' => [
                    'view' => 'offresView',
                    'middlewares' => ["NeedStudent","NeedAdmin"]
                ],
                'add-offre' => [
                    'view' => 'add-offre',
                    'middlewares' => ["NeedAdmin"]
                ],
                'create_jury' => [
                    'view' => 'createJuryForm',
                    'middlewares' => ["NeedAdmin"]
                ],
                'create-salle' => [
                    'view' => 'createSalle',
                    'middlewares' => ["NeedAdmin"]
                ]
            ];
    }

    /**
     * Returns the single instance of this class.
     * @return ApplicationController the single instance of this class.
     */
    public static function getInstance(): ApplicationController
    {
        if (!isset(self::$instance)) {
            self::$instance = new ApplicationController;
        }
        return self::$instance;
    }

    /**
     * Returns the controller that is responsible for processing the request
     * specified as parameter. The controller can be null if their is no data to
     * process.
     * @param Array $request The HTTP request.
     * @param Controller The controller that is responsible for processing the
     * request specified as parameter.
     */
    public function getController($request): null|string
    {
        if ($this->keyExists($request['page'])) {
            return $this->routes[$request['page']]['controller'];
        } else {
            return null;
        }
    }

    /**
     * Returns the view that must be return in response of the HTTP request
     * specified as parameter.
     * @param Array $request The HTTP request.
     * @param Object The view that must be return in response of the HTTP request
     * specified as parameter.
     */
    public function getView($request): string
    {
        if ($this->keyExists($request['page'])) {
            return $this->routes[$request['page']]['view'];
        } else {
            return $this->routes['error']['view'];
        }
    }

    /**
     * Returns the view that must be return in response of the HTTP request
     * specified as parameter.
     * @param Array $request The HTTP request.
     * @param Object The middleware that must be loaded before the HTTP request is handled
     */
    public function getMiddlewares($request): null|array
    {
        if ($this->keyExists($request['page'])) {
            return $this->routes[$request['page']]['middlewares'];
        } else {
            return null;
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function keyExists($key): bool
    {
        return array_key_exists($key, $this->routes);
    }


}

?>
