<?php

namespace App\Controllers;

class ApiController
{

    private static $instance;
    private $routes;

    private string $errorClass="Error";

    private function __construct(){
        //IMPORTANT mettre les routes sans id avant pour éviter que le champ soit considéré comme un string
        $this->routes=[
            'user'=>[
                "class"=>"User", //le nom de la classe
                "methods"=>[
                    "GET"=> [
                        "/"=>"index", //index correspond au nom de la fonction dans la classe User
                        "/types"=>"getTypes", //retourne les types d'utilisateurs
                        "/type/:type"=>"getByType", //index correspond au nom de la fonction dans la classe User
                        "/:id" => "show", //:id est un paramètre qui est récupérable en paramètre de la fonction show
                    ],
                    "POST" => [
                        "/" => "store",
                    ],
                    "PUT" => [
                        "/" => "updateByAction",
                        "/:id" => "updateOne"
                    ],
                    "DELETE" => [
                        "/:id" => "destroy"
                    ]
                ]
            ],
            'auth'=>[
                "class"=>"Auth", //le nom de la classe
                "methods"=>[
                    "POST" => [
                        "/login" => "login",
                        "/register" => "register"
                    ],
                    "GET" => [
                        "/logout" => "logout"
                    ]
                ]
            ],
            'planning' => [
                "class" => "Planning",
                "methods" => [
                    "GET" => [
                        "/" => "getAll",
                        "/:id" => "getOne",
                        "/:id/entretiens" => "getEntretiens",
                        "/:id/jurys" => "getJurys"
                    ],
                    "POST" => [
                        "/jury" => "save_jury"
                    ],
                    "DELETE" => [
                        "/entretiens/:id" => "deleteEntretien"
                    ]
                ]
            ],
            'salle' => [
                "class" => "Salle",
                "methods" => [
                    "GET" => [
                        "/" => "getAll",
                        "/:id" => "getOne"
                    ]
                ]
            ],
            'documents'=>[
                "class"=>"Documents", //le nom de la classe
                "methods"=>[
                    "GET"=>[
                        "/student" => "getAllStudent", //retourne tous les fichiers de l'étudiant connecté
                        "/getStudents" => "getStudents", //retourne tous les documents des etudiants
                        "/student/download/:id" => "downloadByType", //id du type du document
                        "/download/:id" => "downloadById",  //id du document
                        "/offres" => "getOffres",  //id du document
                        "/:id_utilisateur" => "getAllFilesByUserId", //recupere toutes les lignes d'un etudiant
                    ],
                    "POST" => [
                        "/student/upload" => "upload_student", //upload des documents (etudiant)
                        "/upload/:id_utilisateur"=> "upload_offers" //upload offres (admin) :id_utilisateur correspond a l'id_utilisateur
                    ],
                    "DELETE"=>[
                        "/student/:id_type_document"=>"delete_student",
                        "/:id_document"=>"delete"
                    ]
                ]
            ],
            'planning_create'=> [
                "class" =>"PlanningCreation",
                "methods"=>[
                    "POST"=>[
                        "/jury" => "save_jury",
                        "/salle" => "save_salle"
                    ]
                ]
            ]
        ];
    }

    /**
     * Returns the single instance of this class.
     * @return ApiController the single instance of this class.
     */
    public static function getInstance(): ApiController
    {
        if (!isset(self::$instance)) {
            self::$instance = new ApiController();
        }
        return self::$instance;
    }

    /**
     * Permet d'eviter des injections xss
     * @param $request $_REQUEST
     */
    public function sanitizeRequest($request){
        $newRequest=array();


        if(!empty($request)){
            foreach ($request as $key=>$value){
                $newRequest[$key]=htmlspecialchars($value);
            }
        }

        return $newRequest;
    }


    public function getEndpoint($url,$method){

        $results=preg_match_all("/([^\/]+)/",$url,$matches,PREG_UNMATCHED_AS_NULL);

        //TODO autoriser uniquement les chiffres et cahractères alphabétiques ( INJECTION SQL)
        //TODO Modifier la regex ^^^^^^

        //user, 1 , test,2
        $matches=$matches[0];

        $endpoint=array_shift($matches);

        if(array_key_exists($endpoint,$this->routes)){
            $endpoint_routes=$this->routes[$endpoint];
            if(array_key_exists($method,$endpoint_routes["methods"])){
                $methods=$endpoint_routes["methods"][$method];
                foreach ($methods as $path => $name){
                    $res=preg_match_all("/([^\/]+)/",$path,$end_match,PREG_UNMATCHED_AS_NULL);
                    $end_match=$end_match[0];
                    if(sizeof($matches)===$res){
                        $params=array();
                        $index=0;
                        foreach ($end_match as $val){
                            if($val!==$matches[$index]){ //si pas le meme nom verifier s'il s'agit d'un parametre
                                if(str_starts_with($val,':')){
                                    $cleaned_val=preg_replace("/^:/",'',$val);
                                    $params[$cleaned_val]=$matches[$index];
                                }else{
                                    break; //aucun match alors break et path suivant
                                }
                            }
                            $index++; //pas de problème
                        }

                        if($index===$res){
                            $classname=$endpoint_routes["class"];
                            return ["classname"=>$classname,"function"=>$name,"params" => $params];
                        }
                    }
                }
            }
        }

        return ["classname" =>$this->errorClass,"function"=>"index", "params"=>null];
    }

}
