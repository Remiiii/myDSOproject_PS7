<?php

namespace App\Controllers;


class MiddlewareController
{

    private array $middlewares;
    private static $instance;

    private function __construct()
    {
        $this->middlewares = [
            "User" => [
                "type" => "global",
                "middlewares" => ["NeedAdmin"]
            ],
            "Auth" => [
                "type" => "special",
                "middlewares" => [
                    [
                        "function" => "logout",
                        "middlewares" => ["NeedLogged"]
                    ]
                ]
            ],
            "Documents" => [
                "type" => "special",
                "middlewares" => [
                    [
                        "function" => "upload_student",
                        "middlewares" => ["NeedStudent"]
                    ],
                    [
                        "function" => "upload_offers",
                        "middlewares" => ["NeedAdmin"]
                    ],
                    [
                        "function" => "getAllStudent",
                        "middlewares" => ["NeedStudent"]
                    ],
                    [
                        "function" => "getStudents",
                        "middlewares" => ["NeedEntreprise", "NeedAdmin"]
                    ],
                    [
                        "function" => "delete_student",
                        "middlewares" => ["NeedStudent"]
                    ],
                    [
                        "function" => "delete",
                        "middlewares" => ["NeedAdmin"]
                    ],
                    [
                        "function" => "downloadById",
                        "middlewares" => ["NeedAdmin", "NeedStudent", "NeedEntreprise"]
                    ],
                    [
                        "function" => "downloadByType",
                        "middlewares" => ["NeedStudent", "NeedEntreprise"]
                    ],
                    [
                        "function" => "getAllFilesByUserId",
                        "middlewares" => ["NeedAdmin", "NeedEntreprise"]
                    ],
                    [
                        "function" => "getOffres",
                        "middlewares" => ["NeedAdmin", "NeedStudent"]
                    ],

                ]
            ],
                "Planning" => [
                    "type" => "global",
                    "middlewares" => ["NeedAdmin"]
                ],
                "Salle" => [
                    "type" => "global",
                    "middlewares" => ["NeedAdmin"]
                ],
            "PlanningCreation"=>[
                "type" => "global",
                "middlewares" => ["NeedAdmin"]
            ]
        ];
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new MiddlewareController();
        }
        return self::$instance;
    }


    public function getMiddleware(string $classname, string $function)
    {
        if (array_key_exists($classname, $this->middlewares)) {
            $middleware = $this->middlewares[$classname]; //get class name
            $type = $middleware["type"];
            $middleware_class = $middleware["middlewares"]; //get middlewares_ClassNames
            if ($type === 'global') {
                return $middleware_class;
            } else {
                if ($type === 'special') {
                    for ($i = 0; $i < sizeof($middleware_class); $i++) {
                        if ($middleware_class[$i]["function"] === $function) {
                            return $middleware_class[$i]["middlewares"];
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Fait un ou entre tous les middlewares et si tous les middleware renvoient false alors le callback est exécuté
     * @param array|null $middlewares_classes
     * @param callable $callback
     */
    public function applyMiddlewares(?array $middlewares_classes, callable $callback)
    {
        $canAccess = true; //si pas de middleware
        if ($middlewares_classes !== null) {
            $canAccess = false; //si au moins un middleware alors false
            foreach ($middlewares_classes as $name) {
                include('middlewares/' . $name . '.php');
                $middlewareName = '\\App\\middlewares\\' . $name;
                $access = (new $middlewareName)->handle($_REQUEST);
                if ($access === true) {
                    $canAccess = $access; //des qu'un middleware est a true alors passe
                    break;
                }
            }
        }
        if (!$canAccess) {
            try {
                $callback();
            } catch (\JsonException $e) {
                http_response_code($e->getCode());
                echo json_encode(["error" => $e->getMessage()]);
            }
            exit();
        }
    }

}