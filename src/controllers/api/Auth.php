<?php

namespace App\Controllers\Api;
use App\Models\UserModel;

require_once 'models/UserModel.php';
require_once 'controllers/api/BaseEndpoint.php';

class Auth extends BaseEndpoint
{

    private UserModel $user;
    public function __construct($request){
        parent::__construct($request);
        $this->user=new UserModel();
    }
    public function login(){
        $result=$this->user->login($this->request);
        echo json_encode($result);
    }

    public function register(){
        $result=$this->user->register($this->request);
        echo json_encode($result);
    }

    public function logout(){
        if(isset($_SESSION["user"])){
            session_unset(); //vide toutes les variables de session
            echo json_encode(["logged off"]);
        }else{
            echo json_encode("Nothing to logg off");
        }
    }

}