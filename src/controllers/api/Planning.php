<?php

namespace App\Controllers\Api;
use App\Models\PlanningModel;
use App\Models\JuryModel;
use App\Models\Vues\PlanningVue;
use App\Models\EntretienModel;
use App\Models\Vues\EntretienVue;
use App\Models\Vues\JuryVue;

require_once 'controllers/api/BaseEndpoint.php';
require_once 'models/PlanningModel.php';
require_once 'models/JuryModel.php';
require_once 'models/vues/PlanningVue.php';
require_once 'models/EntretienModel.php';
require_once 'models/vues/EntretienVue.php';
require_once 'models/vues/JuryVue.php';

class Planning extends BaseEndpoint {
    private PlanningModel $planning;
    private JuryModel $jury;
    private PlanningVue $planningVue;
    private EntretienModel $entretien;
    private EntretienVue $entretienVue;
    private JuryVue $juryVue;

    public function __construct($request) {
        parent::__construct($request);
        $this->planning = new PlanningModel();
        $this->jury = new JuryModel();
        $this->planningVue = new PlanningVue();
        $this->entretien = new EntretienModel();
        $this->entretienVue = new EntretienVue();
        $this->juryVue = new JuryVue();
    }

    public function getAll() {
        $data = $this->planningVue->getAll();
        echo json_encode($data);
    }

    public function getOne($params) {
        // c'est l'id de l'appel à l'api, pas l'id de la bdd
        $data = $this->planningVue->findByParam("id_planning", $params["id"]);
        echo json_encode($data);
    }

    public function getEntretiens($params) {
        $data = $this->entretienVue->findByParam("id_planning", $params["id"]);
        echo json_encode($data);
    }

    public function deleteEntretien($params) {
        $data = $this->entretien->delete($params["id"]);
        echo json_encode($data);
    }

    public function getJurys($params) {
        $data = $this->juryVue->findByParam("id_planning", $params["id"]);
        echo json_encode($data);
    }
}
