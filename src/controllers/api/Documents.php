<?php

namespace App\Controllers\Api;

use App\Models\DocumentModel;
use App\Models\TypeDocumentModel;
use App\Models\UserModel;
use App\Models\Vues\UtilisateurVue;
use App\Models\Vues\DocumentVue;
use App\Models\Vues\VueEtudiant;

require_once 'controllers/api/BaseEndpoint.php';
require_once 'models/DocumentModel.php';
require_once 'models/TypeDocumentModel.php';
require_once 'models/vues/UtilisateurVue.php';
require_once 'models/vues/VueEtudiant.php';
require_once 'models/vues/DocumentVue.php';
require_once 'models/UserModel.php';

class Documents extends BaseEndpoint
{
    private DocumentModel $model;
    private DocumentVue $documentVue;
    private VueEtudiant $vueEtudiant;
    private UserModel $userModel;
    private TypeDocumentModel $type_model;

    public function __construct($request)
    {
        parent::__construct($request);
        $this->model = new DocumentModel();
        $this->vueEtudiant = new VueEtudiant();
        $this->type_model = new TypeDocumentModel();
        $this->documentVue = new DocumentVue();
        $this->userModel = new UserModel();
    }

    public function getAllStudent()
    {
        $id_utilisateur = $_SESSION["user"]->id_utilisateur;
        $result = $this->model->findByParam("id_utilisateur", $id_utilisateur);
        echo json_encode($result);
    }

    public function getStudents()
    {
        $result = $this->documentVue->findByParam("etudiant", 1);
        echo json_encode($result);
    }

    public function getAllFilesByUserId($params)
    {
        $id = $params["id_utilisateur"];

        $result = $this->vueEtudiant->findByParam("id_utilisateur", $id); //regarde si il s'agit d'un etudiant
        if ($result["status"] === true) {
            $result = $this->documentVue->findByParam("id_utilisateur", $id);
        }
        echo json_encode($result);
    }

    /**
     * renvoie les offres de stages
     * @throws \Exception
     */
    public function getOffres()
    {
        $result = $this->documentVue->findByParam("id_type_document", 4); //4 offre de stage
        echo json_encode($result);
    }

    public function upload_student()
    {
        $document = [
            "id_utilisateur" => $_SESSION["user"]->id_utilisateur,
            "id_type_document" => null
        ];

        //TODO Verifier que les id des types autorisés correspondent aux documents possible en fonction du type de l'utilisateur
        $authorized_types = [1, 2, 3];
        $documents = [];

        foreach ($_FILES as $type => $file) {
            if (in_array($type, $authorized_types)) {
                if (!empty($file) && $file["size"] > 0) {
                    $result = $this->model->isValidDocument($file, $type, true);

                    if ($result["status"] === true) {
                        $doc = $document;
                        $doc["id_type_document"] = $type;
                        $inserted = $this->model->insert(
                            $doc,
                            true
                        );
                        if ($inserted["status"] === true) {
                            $id = $inserted["id"];
                            $moved = $this->model->moveTmpFileToDocuments($file["tmp_name"], $id, $file["type"]);
                            if (!$moved) {
                                $this->model->delete($id);
                            }
                            unset($inserted["id"]);
                        }
                        $documents[$type] = $inserted;
                    } else {
                        $documents[$type] = $result;
                        break;
                    }
                }
            }
        }
        echo json_encode($documents);
    }

    /**
     * @throws \JsonException
     * @throws \Exception
     */
    public function upload_offers($params)
    {

        $id_utilisateur = $params['id_utilisateur'];

        $user = $this->userModel->find($id_utilisateur);
        if ($user["status"] === false) {
            throw new \JsonException("L'id utilisateur n'existe pas", 400);
        }
        if(intval($user["data"]->id_type_utilisateur)!==3){ //3 pour type entreprise
            throw new \JsonException("L'utilisateur ne peut pas avoir d'offres", 400);
        }

        $document = [
            "id_utilisateur" => $id_utilisateur,
            "id_type_document" => 4
        ];

        //TODO Verifier que les id des types autorisés correspondent aux documents possible en fonction du type de l'utilisateur

        $nb_inserted=0;

        foreach ($_FILES as $key=>$f) {
            $files=[];
            foreach ($f as $n=>$value){
                foreach ($value as $idx=>$val){
                    $files[$idx][$n]=$val;
                }
            }

            foreach ($files as $idx=>$file){
                if (!empty($file) && $file["size"] > 0) {
                    $result = $this->model->isValidDocument($file, $file["type"], false);

                    if ($result["status"] === true) {
                        $inserted = $this->model->insert(
                            $document,
                            true
                        );
                        if ($inserted["status"] === true) {
                            $id = $inserted["id"];
                            $moved = $this->model->moveTmpFileToDocuments($file["tmp_name"], $id, $file["type"]);
                            if (!$moved) {
                                $this->model->delete($id);
                            }else{
                                $nb_inserted+= 1;
                            }
                        }
                    } else {
                        break;
                    }
                }
            }

        }
        echo json_encode(["insertedCount"=>$nb_inserted]);
    }

    /**
     * @throws \JsonException
     */
    public function downloadByType($params)
    {
        $id = $params["id"];

        $documents = $this->model->findByMultipleParams(["id_utilisateur", "id_type_document"],
            [$_SESSION["user"]->id_utilisateur, $id]);
        if ($documents["status"] === true) {
            $this->downloadById(["id" => $documents["data"][0]->id_document]);
        }
        throw new \JsonException("Le type n'existe pas");
    }

    /**
     * @throws \JsonException
     */
    public function downloadById($params)
    {
        $id = $params["id"];

        $document = $this->model->find($id); //récupere l'objet document

        $user = $_SESSION["user"];
        $is_student = (bool)$user->etudiant;

        if ($document["status"] === true) {
            $vue_utilisateur = new UtilisateurVue();  //pour recuperer le proprioetaire du document
            $doc_proprio = $vue_utilisateur->findByParam("id_utilisateur", $document["data"]->id_utilisateur);
            if ($doc_proprio["status"] === true) {
                $proprio = $doc_proprio["data"][0];
                $is_student_proprio = (bool)$proprio->etudiant; //seulement dans la vue Utilisateur

                //Verifie que le document n'est pas du même type utilisateur ( un etudiant ne doit pas voir les cv des autres etudiants, idem pour entreprise)
                if ($user->id_utilisateur !== $proprio->id_utilisateur && ($is_student === true && $is_student_proprio === true || $proprio->id_type_utilisateur === $user->id_type_utilisateur)) {
                    throw new \JsonException("can't access this resource", 401);
                }
            }


            $type_res = $this->type_model->find($document["data"]->id_type_document); //recupere l'object type du document
            if ($type_res["status"] === true) {
                $this->model->downloadFile($document["data"], $user);
            }
        }
        throw new \JsonException("Le type ou le document n'existe pas");
    }

    /**
     * @throws \JsonException
     */
    public function delete_student($params)
    {
        $id_type_document = $params["id_type_document"];

        $result = $this->model->findByMultipleParams(
            ["id_utilisateur", "id_type_document"],
            [$_SESSION["user"]->id_utilisateur, $id_type_document]
        );

        if ($result["status"] === true) {
            $result = $this->model->removeDocument($result["data"][0]->id_document);
        } else {
            throw new \JsonException("File type not found: ", 400);
        }

        echo json_encode($result);
    }

    public function delete($params)
    {
        $id_document = $params["id_document"];

        $ret = $this->model->removeDocument($id_document);

        echo json_encode($ret);
    }
}