<?php

namespace App\Controllers\Api;

require_once 'controllers/api/BaseEndpoint.php';

class Error extends BaseEndpoint
{
    public function index(){
        throw new \JsonException("The given path doesn't exist",400);
    }
}