<?php

namespace App\Controllers\Api;

use App\Models\JuryModel;
use App\Models\SalleModel;

require_once 'controllers/api/BaseEndpoint.php';
require_once 'models/JuryModel.php';
require_once 'models/SalleModel.php';

class PlanningCreation extends BaseEndpoint
{
    private JuryModel $jury;
    private SalleModel $salle;

    public function __construct($request) {
        parent::__construct($request);
        $this->jury = new JuryModel();
        $this->salle = new SalleModel();

    }

    private function convertDate($date){
        return date('Y-m-d H:i:s', strtotime($date));
    }

    private function checkDates($date_deb,$date_fin){
        if($date_fin>$date_deb){
            return true;
        }
        return false;
    }

    /**
     * @throws \JsonException
     */
    public function save_jury(){
        $date_deb=$this->request["date_deb"];
        $date_fin=$this->request["date_fin"];

        $new_date_deb = $this->convertDate($date_deb);
        $new_date_fin = $this->convertDate($date_fin);

        $result=["status"=>false,"message"=>"La date de fin doit être supérieur à la date de début"];
        if($this->checkDates($new_date_deb,$new_date_fin)){
            $this->request["date_deb"]=$new_date_deb;
            $this->request["date_fin"]=$new_date_fin;

            $result = $this->jury->insert($this->request);
        }
        echo json_encode($result);
    }

    public function save_salle(){
        $date_deb=$this->request["date_deb"];
        $date_fin=$this->request["date_fin"];

        $new_date_deb = $this->convertDate($date_deb);
        $new_date_fin = $this->convertDate($date_fin);

        $result=["status"=>false,"message"=>"La date de fin doit être supérieur à la date de début"];
        if($this->checkDates($new_date_deb,$new_date_fin)){
            $this->request["date_deb"]=$new_date_deb;
            $this->request["date_fin"]=$new_date_fin;
            $result = $this->salle->insert($this->request);
        }
        echo json_encode($result);
    }

    public function planning_create(){
      // Ceci est un commentaire sur une seule ligne
      # Ceci est un autre commentaire sur une seule ligne
      /* Ceci est un commentaire sur
       plusieurs lignes */
    }

}
