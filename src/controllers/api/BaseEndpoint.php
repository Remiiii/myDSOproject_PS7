<?php

namespace App\Controllers\Api;

abstract class BaseEndpoint
{
    protected $request;

    public function __construct($request){
        $this->request=$request;
    }
}