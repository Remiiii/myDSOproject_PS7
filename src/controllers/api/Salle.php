<?php

namespace App\Controllers\Api;
use App\Models\SalleModel;

require_once 'controllers/api/BaseEndpoint.php';
require_once 'models/SalleModel.php';

class Salle extends BaseEndpoint {
    private SalleModel $salle;

    public function __construct($request) {
        parent::__construct($request);
        $this->salle = new SalleModel();
    }

    public function getAll() {
        $data = $this->salle->getAll();
        echo json_encode($data);
    }

    public function getOne($params) {
        // c'est l'id de l'appel à l'api, pas l'id de la bdd
        $data = $this->salle->findByParam("id_salle", $params["id"]);
        echo json_encode($data);
    }

}
