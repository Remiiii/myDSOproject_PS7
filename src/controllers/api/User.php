<?php

namespace App\Controllers\Api;
use App\Models\DocumentModel;
use App\Models\TypeUtilisateurModel;
use App\Models\UserModel;
use App\Models\Vues\UtilisateurVue;

require_once 'controllers/api/BaseEndpoint.php';
require_once 'models/UserModel.php';
require_once 'models/vues/UtilisateurVue.php';
require_once 'models/TypeUtilisateurModel.php';
require_once 'models/DocumentModel.php';

class User extends BaseEndpoint
{
    private UserModel $user;
    private UtilisateurVue $userVue;
    private DocumentModel $documentModel;
    private TypeUtilisateurModel $typeUtilisateurModel;

    public function __construct($request){
        parent::__construct($request);
        $this->user=new UserModel();
        $this->userVue=new UtilisateurVue();
        $this->documentModel=new DocumentModel();
        $this->typeUtilisateurModel=new TypeUtilisateurModel();
    }

    public function index(){
        $data=$this->userVue->getAll();
        echo json_encode($data);
    }

    public function show($params){
        $data=$this->userVue->findByParam("id_utilisateur",$params["id"]);
        echo json_encode($data);
    }

    public function store(){
        $result=$this->user->insertAdmin($this->request);
        echo json_encode($result);
    }

    public function updateOne($params){
        $id=$params["id"];

        $result=$this->user->updateOne($id,$this->request);
        echo json_encode($result);
    }

    /**
     * @throws \Exception
     */
    public function getTypes(){
        echo json_encode($this->typeUtilisateurModel->getAll());
    }

    // ONLY FOR TESTINT API
    public function getByType($params){
        $result=$this->userVue->findByParam("type_utilisateur",$params["type"]);
        echo json_encode($result);
    }

    public function updateByAction(){
        $action_id=$this->request["action_id"];
        $validate=empty($this->request['validate'])?0:1;
        $id_type_utilisateur=$this->request["id_type_utilisateur"];
        $ids_string=$this->request["ids"];
        $ids=array_map('intval',explode(',',$ids_string));

        switch ($action_id){
            case 0:{
                array_map(array($this->user,'delete'),$ids);
                break;
            }
            case 1:{
                $param=["valide"=>$validate];
                foreach ($ids as $id){
                    $this->user->updateOne($id,$param);
                }
                break;
            }
            case 2:{
                $param=["id_type_utilisateur"=>$id_type_utilisateur];
                foreach ($ids as $id){
                    $this->user->updateOne($id,$param);
                }
                break;
            }
        }
        echo json_encode($ids);
    }

    /**
     * @throws \JsonException
     */
    public function destroy($params){


        $id_utilisateur=$params["id"];

        $doc_result=$this->documentModel->findByParam("id_utilisateur",$id_utilisateur);
        if($doc_result["status"]===true){
            $documents=$doc_result["data"];
            foreach ($documents as $doc){
                $deleted_doc=$this->documentModel->removeDocument($doc->id_document);
            }
        }
        $rowsDeleted=$this->user->delete($id_utilisateur);

        if($id_utilisateur===$_SESSION["user"]->id_utilisateur){
            session_destroy();
        }
        echo json_encode($rowsDeleted);
    }
}