<?php

namespace App;

require_once ('config/environment.php');
ini_set( 'session.cookie_httponly', 1 );

if (function_exists('header_remove')) {
    header_remove('X-Powered-By'); // PHP 5.3+
} else {
    @ini_set('expose_php', 'off');
}

header("Content-Security-Policy: default-src 'self'");
session_set_cookie_params(["SameSite" => "Strict"]); //none, lax, strict
//session_set_cookie_params(["Secure" => "false"]); //false, true
session_set_cookie_params(["HttpOnly" => "true"]); //false, true
header('X-Content-Type-Options: nosniff');
header("X-Frame-Options: SAMEORIGIN");

header('Access-Control-Allow-Origin: '.DOMAIN_URL); //modifier avec domaine de production
header("X-XSS-PROTECTION: 0");

session_start();

require_once('controllers/ApplicationController.php');
require_once('controllers/ApiController.php');

use App\Controllers\ApplicationController;
use App\Controllers\ApiController;
use App\Controllers\MiddlewareController;
use App\Middlewares\BaseMiddleware;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

define("HTTP_URL", '/index.php?page=');
define("API_URL", '/api');

if (empty($_REQUEST["page"])) {
    $_REQUEST['page'] = 'home';
}

//check if url is for the api or not
$url = $_SERVER["REQUEST_URI"];

if (str_starts_with($url, "/api")) {
    header('Content-Type: application/json; charset=utf-8');

    $url = preg_replace('/^\/api/', '', $url); //enleve /api de l'url
    $method = $_SERVER["REQUEST_METHOD"];

    $apiController = ApiController::getInstance();
    $apiClass = $apiController->getEndpoint($url,$method); //retourne la classe, la fonction et les parametres s'il y en a

    $classname=$apiClass["classname"];
    $function=$apiClass["function"];
    $params=$apiClass["params"];

    require_once "controllers/MiddlewareController.php";
    $middlewares_instance=MiddlewareController::getInstance();

    $middlewares=$middlewares_instance->getMiddleware($classname,$function);

    $middlewares_instance->applyMiddlewares($middlewares,function(){
        throw new \JsonException("Unauthorized",400);
    });

    $apiClassName="\\App\\Controllers\\Api\\".$classname;
    include ('controllers/api/'.$classname.".php");

    parse_str(file_get_contents("php://input"),$_REQUEST);
    try{
        $params=$apiController->sanitizeRequest($params);
        $_REQUEST=$apiController->sanitizeRequest($_REQUEST);

        (new $apiClassName($_REQUEST))->$function($params);
    }catch (\JsonException $e){ //throw JsonException dans les Api
        http_response_code($e->getCode());
        echo json_encode(["error"=>$e->getMessage()]);
    }catch (\PDOException $e){
        http_response_code(400);
        //TODO changer le message
        echo json_encode(["error"=>$e->getMessage()]);
    }catch (\Exception $e){
        http_response_code(400);
        echo json_encode(["error"=>$e->getMessage()]);
    }

}else{
    header('Content-Type: text/html; charset=utf-8');

    $instance = ApplicationController::getInstance();

    $middlewares = $instance->getMiddlewares($_REQUEST);

    require_once "controllers/MiddlewareController.php";
    $middlewares_instance=MiddlewareController::getInstance();

    $middlewares_instance->applyMiddlewares($middlewares,function () use ($instance) {
        BaseMiddleware::redirect($instance::$DEFAULT_HOME);
    });

    $view = $instance->getView($_REQUEST);
    if ($view != null) {
        $file_path='views/pages/' . $view . '.php';
        if(file_exists($file_path)){
            include($file_path);
        }
    }

}

