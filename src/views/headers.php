<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="views/css/common.css" type="text/css">
    <link rel="stylesheet" href="views/css/normalize.css" type="text/css">
    <script type="application/javascript" src="views/js/utils.js"></script>
    <script type="application/javascript" src="views/js/libraries/jquery.min.js"></script>
    <script type="application/javascript" src="views/js/navbar.js"></script>
    <title><?= $title ?></title>
</head>