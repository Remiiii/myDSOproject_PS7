<?php ob_start(); ?>
<link rel="stylesheet" href="views/css/planning.css">

<script src="views/js/planning.js" defer></script>

<?php $headers = ob_get_clean() ?>

<?php ob_start() ?>

<!-- L'ID DE PLANNING DOIT ÊTRE PASSÉ EN get PAR LA VARIABLE id ! -->

<div id="flex-container" class="flex-center">
    <div class="center-box">
        <!-- récupération de l'id du planning à afficher ici -->
        <h1 id="title" data-id="<?php echo (isset($_GET["id"]) and !empty($_GET["id"]) and is_numeric($_GET["id"])) ? $_GET["id"] : "id non défini" ?>">Planning</h1>
        <div class="hidden bubble" data-show-hover="1-10-2021:8-0">Pouet</div>

        <div id="error" class="hidden">
            <p id="errormessage"></p>
        </div>

        <div id="planning" class="hidden">
        </div>
    </div>
</div>

<?php $content = ob_get_clean() ?>

<?php
$title = "Planning";
require_once('views/templates/main.php');
?>
