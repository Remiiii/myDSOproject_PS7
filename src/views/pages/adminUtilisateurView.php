<?php use App\Models\TypeUtilisateurModel;

ob_start(); ?>

<link rel="stylesheet" href="views/css/table.css"/>
<link rel="stylesheet" href="views/css/modal.css"/>
<link rel="stylesheet" href="views/css/loginPage.css"/>
<link rel="stylesheet" href="views/css/adminUtilisateurPage.css"/>
<script src="views/js/adminUtilisateur.js" defer></script>

<?php $headers = ob_get_clean() ?>

<?php
require_once 'models/TypeUtilisateurModel.php';
$model = new TypeUtilisateurModel();

$types = $model->getAll();
ob_start();
?>
<div id="container" class="shadow-box">

    <div id="modal" class="modal hide">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">X</span>

            <form id="update_form" class="custom_form">
                <div class="form_label">
                    <label for="nom">Nom*</label>
                    <input name="nom" type="text" placeholder="Nom" required/>
                </div>
                <div class="form_label">
                    <label for="prenom">Prénom*</label>
                    <input name="prenom" type="text" placeholder="Prénom" required/>
                </div>

                <div class="form_label">
                    <label for="mail">Email*</label>
                    <input name="mail" type="email" placeholder="Adresse mail" required/>
                </div>

                <div class="form_label">
                    <label for="type_modal">Type </label>
                    <select id="type_modal" name="id_type_utilisateur" form="update_form">
                        <?php foreach ($types as $type) {
                            $id_type = $type->id_type_utilisateur;
                            $libelle = $type->libelle;
                            ?>
                            <option value="<?= $id_type ?>"><?= $libelle ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form_label checkbox">
                    <label for="valide">Valider</label>
                    <input name="valide" value="0" type="hidden"/>
                    <input name="valide" id="valide" type="checkbox" placeholder="Valide">
                </div>

                <input type="submit" value="Valider la/les modification(s)" title="Valider et créer le compte"/>
            </form>
        </div>
    </div>


    <form id="action_bar" class="action_bar">
        <h2>
            Action de groupe
        </h2>
        <div class="center-flex">
            <p>
                Ligne selectionnée(s): <span id="nbLigne">0</span>
            </p>
        </div>
        <select name="action_id">
            <option value="0">
                Supprimer
            </option>
            <option value="1">
                Valider
            </option>
            <option value="2">
                Modifier Type utilisateur
            </option>
        </select>

        <input id="action_bar_checkbox" class="centerbox hide" class="hide" type="checkbox" value="0" name="validate">

        <select class="hide" id="type_modification" name="id_type_utilisateur">
            <?php foreach ($types as $type) {
                $id_type = $type->id_type_utilisateur;
                $libelle = $type->libelle;
                ?>
                <option value=<?= $id_type ?>><?= $libelle ?></option>
            <?php } ?>
        </select>

        <input type="submit" value="Valider">
    </form>


    <form id="user_add_form" class="custom_form user_add_form" method="POST" action="/api/user">
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Mail</th>
                <th>Type utilisateur</th>
                <th>Mot de passe</th>
                <th>Confirmation Mot de passe</th>
                <th>Valide</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input name="nom" type="text" placeholder="Nom" required/>
                </td>
                <td>
                    <input name="prenom" type="text" placeholder="Prénom" required/>
                </td>
                <td>
                    <input name="mail" type="email" placeholder="Adresse mail" required/>

                </td>
                <td>
                    <select name="id_type_utilisateur" form="user_add_form">
                        <?php foreach ($types as $type) {
                            $id_type = $type->id_type_utilisateur;
                            $libelle = $type->libelle;
                            ?>
                            <option value="<?= $id_type ?>"><?= $libelle ?></option>
                        <?php } ?>
                    </select>
                </td>

                <td>
                    <input name="mot_de_passe" id="mot_de_passe" type="password" minlength="8"
                           placeholder=">8 caractères dont minuscule, majuscule, chiffre, caractère spécial"
                           required/>
                </td>
                <td>
                    <input name="password_confirmation" id="password_confirmation" minlength="8" type="password"
                           placeholder="Confirmer le mot de passe" required/>
                </td>
                <td class="center">
                    <input type="hidden" name="valide" value="0">
                    <input type="checkbox" name="valide" value="0">
                </td>
                <td>
                    <input id="add_user_button" class="btn" type="submit" value="+"/>
                </td>
                <td>
                    <input id="cancel_user_button" class="btn" type="button" value="-"/>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="custom_msg hide"></div>

    </form>

    <table id="user_table" class="table_container">
    </table>

</div>

<?php $content = ob_get_clean() ?>

<?php
$title = "Administration utilisateurs";
require_once('views/templates/main.php');
?>
