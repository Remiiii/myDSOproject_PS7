<?php ob_start();?>
<link rel="stylesheet" href="views/css/createSalle.css">


<script src="views/js/createSalle.js" defer></script>

<?php $headers=ob_get_clean() ?>

<?php ob_start() ?>
<div class="container">
	<h1 class="titre">Création de Salle</h1>
	<div class="createSalle">     
		
		<form class="addSalle" id="form">
			<div class="options">
			    <input type="text" placeholder="Nom de la salle" id="name" class="option" name="nom" required>
			    <input type="datetime-local" value="2018-06-12T19:30" id="date-deb" class="option" name="date_deb" required>
			    <input type="datetime-local" value="2018-06-13T19:30" id="date-fin" class="option" name="date_fin" required>    
			</div>
			<div class="btn">
				<input type="submit" value="Créer Salle" class="btn">
			</div>
		</form>
	</div>
</div>

<?php $content=ob_get_clean() ?>
<?php
$title="Stages - Accueil";
require_once ('views/templates/main.php');
?>