<?php
session_unset(); //enleve variables de sessions
ob_start();?>
<link rel="stylesheet" href="views/css/loginPage.css">
<script src="views/js/register.js" defer></script>

<?php
$headers=ob_get_clean() ;

$possible_types=[7=>"Candidat en Cyberdata",6=>"CyberLog5",5=>"CyberLog4",4=>"Exterieur"];

ob_start() ?>
<div class="flex-center">
    <div class="wrapper">
        <div class="form_wrapper">
            <div class="form_content">
                <h1>Créer un compte</h1>
                <form id="register_form" class="custom_form" action=<?= API_URL . '/auth/register' ?>>
                    <div class="form_label">
                        <label for="nom">Nom*</label>
                        <input name="nom" id="nom" type="text" placeholder="Nom" required/>
                    </div>
                    <div class="form_label">
                        <label for="prenom">Prénom*</label>
                        <input name="prenom" id="prenom" type="text" placeholder="Prénom" required/>
                    </div>

                    <div class="form_label">
                        <label for="mail">Email*</label>
                        <input name="mail" id="mail" type="email" placeholder="Adresse mail" required/>
                    </div>
                    <div class="form_label">
                        <label for="mot_de_passe">Mot de passe*</label>
                        <input name="mot_de_passe" id="mot_de_passe" type="password" minlength="8" placeholder=">8 caractères dont minuscule, majuscule, chiffre, caractère spécial" required/>
                    </div>
                    <div class="form_label">
                        <label for="password_confirmation">Confirmer le mot de passe*</label>
                        <input name="password_confirmation" id="password_confirmation" minlength="8" type="password" placeholder="Confirmer le mot de passe" required/>
                    </div>
                    <div class="form_label">
                        <label for="type">Je suis </label>
                        <select name="id_type_utilisateur" form="register_form">
                            <?php foreach ($possible_types as $id=>$label) { ?>
                            <option value="<?= $id ?>"><?= $label?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <span class="errorMsg hide"></span>

                    <input type="submit" value="Valider et créer le compte" title="Valider et créer le compte"/>
                    <div class="bottom_form">
                        <div>Déja un compte ? <a href=<?= HTTP_URL . 'login_form' ?>>Se connecter</a></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$title = "Créer un compte";
$content = ob_get_clean();
require_once('views/templates/main.php')
?>
