<?php ob_start();?>

<link rel="stylesheet" href="views/css/offres.css"/>


<script src="views/js/offres.js" defer></script>
<script src="views/js/libraries/bootstrap.min.js"></script>

<?php $headers=ob_get_clean() ?>

<?php 
$liste_id_etudiant = array(5); // Liste de tous les id des etudiants

if(isset($_GET['id'])){ // On récupère l'id du GET s'il existe
   $id_etud = htmlspecialchars($_GET['id']); 
}else{
   $id_etud = "";                         
}

$url_api = API_URL."/documents/download/";   // On enregistre l'url de l'API
// $url_end = ".pdf#toolbar=0&navpanes=0&scrollbar=0"; // Paramètrage de l'affichage des pdf, barre d'outils en haut

?>

<?php ob_start() ?>
    <div id="container" data-url="<?= $url_api?>"> 
        <div class="nav">
            <div class="btn">
                <input type="image" src="views/img/arrow_left.png" class="button-nav" id="button-prec">
            </div>
            <h1 class="mb-3" id="titre">Offre de </h1>
            <div class="btn">
                <input type="image" src="views/img/arrow_right.png" class="button-nav" id="button-suiv">
            </div>
        </div>
        <div class="offre">
            <embed id="offre" src="" height="1043" width="700"/>        
        </div>
    </div>

<div id="empty_list">
    <h1> Aucun document n'a été déposé par les entreprises ! </h1> 
</div>
<?php $content=ob_get_clean() ?>

<?php
$title="Stages - Accueil";
require_once ('views/templates/main.php');
?>
