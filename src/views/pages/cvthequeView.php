<?php ob_start();?>

<link rel="stylesheet" href="views/css/libraries/bootstrap.min.css"/>
<link rel="stylesheet" href="views/css/cvtheque.css"/>


<script src="views/js/cvtheque.js" defer></script>
<script src="views/js/libraries/bootstrap.min.js"></script>

<?php $headers=ob_get_clean() ?>

<?php 
$liste_id_etudiant = array(5); // Liste de tous les id des etudiants

if(isset($_GET['id'])){ // On récupère l'id du GET s'il existe
   $id_etud = htmlspecialchars($_GET['id']); 
}else{
   $id_etud = "";                        
}

$url_api = API_URL."/documents/download/";   // On enregistre l'url de l'API
// $url_end = ".pdf#toolbar=0&navpanes=0&scrollbar=0"; // Paramètrage de l'affichage des pdf, barre d'outils en haut

?>

<?php ob_start() ?>
   <div class="container"> 
      <div class="nav">
         <div class="bouton-prec">
            <input type="image" src="views/img/arrow_left.png" class="button-nav" id="button-prec">
         </div>
         <div class="bouton-liste">
            <input type="button" value="Liste des étudiants" class="button-liste" id="button-liste">
         </div>
         <div class="bouton-suiv">
            <input type="image" src="views/img/arrow_right.png" class="button-nav" id="button-suiv">
         </div>
         
      </div>   
      <h1 class="mb-3" id="titre">CVthèque de </h1>
      
      <div id="myCarousel" class="carousel slide border" data-interval="false" data-id="<?= $id_etud?>" data-url="<?= $url_api?>">
         <!-- Carousel -->
         <div class="carousel-inner" height="100%">
            <div class="item active">
                  <embed id="cv" src="" height="1043" width="700"/>
            </div>
            <div class="item">
                  <embed id="lm" src="" height="1043" width="700"/>
            </div>
            <div class="item">
                  <embed id="lr" src="" height="1043" width="700"/>
            </div>
         </div>
         <!-- Left and right controls -->
         <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <input type="image" src="views/img/arrow_left.png" class="glyphicon glyphicon-chevron-left">
            <span class="sr-only">Previous</span>
         </a>
         <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="sr-only">Next</span>            
            <input type="image" src="views/img/arrow_right.png" class="glyphicon glyphicon-chevron-right">
         </a>
      </div>
   </div>   

<?php $content=ob_get_clean() ?>

<?php
$title="Stages - Accueil";
require_once ('views/templates/main.php');
?>
