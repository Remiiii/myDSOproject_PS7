<?php

use App\Models\Vues\DocumentVue;

$user = $_SESSION["user"];
$is_student = (bool)$user->etudiant;
$is_entreprise = ($user->type_utilisateur === "Entreprise");

if ($is_student === false && $is_entreprise === false) {
    echo "pas autorisé";
    exit(400);
}
?>

<div id="document_container">
    <h1 id="doc_title">
        Mes documents
    </h1>

    <?php if ($is_student) {
        $data_types = [1 => 'CV', 2 => 'Lettre de motivation', 3 => 'Lettre de recommandation'];
        ?>
        <div id="student_div">
            <div id="existing_files" class="div_file_container hide">
                <h2>Voir mes documents</h2>
                <table>
                    <?php foreach ($data_types as $type => $label) { ?>
                        <tr class="file_display hide" data-type="<?= $type ?>" data-label="<?= $label ?>">
                            <td class="label_type"><?= $label ?></td>
                            <td>
                                <div class="right_display">
                                    <input class="voir" type="image" src="views/img/dashboard/see.png" alt=""/>
                                    <input class="supprimer" type="image" src="views/img/dashboard/delete.png"/>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <form id="document_form" class="document_form div_file_container">
                <h2>Ajouter des documents</h2>
                <table>
                    <?php foreach ($data_types as $type => $label) { ?>
                        <tr class="file_display" data-type="<?= $type ?>" data-label="<?= $label ?>">
                            <td class="label_type"><?= $label ?></td>
                            <td>
                                <label for="<?= $type ?>" class="label_file_upload"><?= $label ?></label>
                                <input id="<?= $type ?>" type="file" name="<?= $type ?>" accept="application/pdf"/>
                            </td>
                            <td class="right_display">
                                <input class="cancel hide" type="image" src="views/img/dashboard/delete.png"/>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
                <span class="errorMsg hide"></span>
                <input type="submit" value="Envoyer le(s) document(s)"/>
            </form>
        </div>
    <?php } else {
        if ($is_entreprise) {
            require_once 'models/vues/DocumentVue.php';

            $document_vue = new DocumentVue();
            $docs = $document_vue->findByParam("id_utilisateur", $user->id_utilisateur);

            if ($docs["status"] === true) { ?>
                <div id="offres_container">

                    <?php foreach ($docs["data"] as $key => $values) {
                        $dl_url="/api/documents/download/" . $values->id_document;
                        ?>

                        <div class="offre_ligne">

                            <h2 class="label_type">
                                <?= "Offre N°  " . $key + 1 ?>
                            </h2>

                            <a class="offre_ligne" href="<?=$dl_url ?>"  target="_blank">
                                <p>Voir l'offre</p>
                                <img class="voir" src="/views/img/dashboard/see.png" alt=""/>
                            </a>

                        </div>

                    <?php } ?>
                </div>
                <?php
            } else {
                ?>
                <div class="errorMsg">
                    Vous n'avez pas d'offres
                </div>


            <?php }
        }
    } ?>


</div>