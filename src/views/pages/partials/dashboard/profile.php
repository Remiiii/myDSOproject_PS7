<?php

$user = $_SESSION["user"];

$data = [
    "nom" => $user->nom,
    "prenom" => $user->prenom,
    "email" => $user->mail,
    "status" => $user->type_utilisateur
]
?>


<div class="profile_container">

    <h1>Mes informations</h1>


        <table class="info_table">
            <?php foreach ($data as $key => $value) { ?>
                <tr class="info_ligne">
                    <td class="info_nom"><?= $key ?></td>
                    <td class="info_val"><?= $value ?></td>
                </tr>

            <?php } ?>
        </table>



</div>


