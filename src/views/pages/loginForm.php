<?php
session_unset(); //enleve variables de sessions
ob_start();?>
<link rel="stylesheet" href="views/css/loginPage.css">
<script src="views/js/login.js"></script>


<?php $headers=ob_get_clean() ?>

<?php

ob_start() ?>
<div class="flex-center">
    <div class="wrapper">
        <div class="form_wrapper">
            <div class="form_content">
                <h1>Se connecter</h1>
                <form id="login_form" class="custom_form" action=<?= API_URL."/auth/login" ?>>
                    <div class="form_label">
                        <label for="mail">Email</label>
                        <input name="mail" id="mail" type="email" placeholder="Adresse mail" required autocomplete="on" autofocus/>
                    </div>
                    <div class="form_label">
                        <label for="mot_de_passe">Password</label>
                        <input name="mot_de_passe" id="mot_de_passe" type="password" placeholder="Mot de passe" autocomplete="on"/>
                    </div>
                    <span class="errorMsg hide"></span>
                    <input id="submit_button" type="submit" value="Se connecter" title="Se connecter" />
                    <div class="bottom_form">
                        <div>Pas encore de compte ? <a href=<?= HTTP_URL.'create_account_form' ?>>S'inscrire</a></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<?php
$content = ob_get_clean();
$title = "LoginPage";
require_once('views/templates/main.php')
?>
