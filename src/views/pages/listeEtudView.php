<?php ob_start();?>
<link rel="stylesheet" href="views/css/listeEtudPage.css">
<link rel="stylesheet" href="views/css/table.css">


<script src="views/js/liste-etud.js" defer></script>

<?php $headers=ob_get_clean() ?>

<?php ob_start() ?>
<div class="container" id="container"> 
    <table id="donnee">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Filière</th>
                <th>Voir CV</th>
            </tr>
        </thead>
        <tbody class="tbody">

        </tbody>
    </table>
</div>

<div id="empty_list">
    <h1> Aucun document n'a été déposé par les élèves ! </h1> 
</div>


<?php $content=ob_get_clean() ?>
<?php
$title="Stages - Accueil";
require_once ('views/templates/main.php');
?>