
<?php ob_start(); ?>
<link rel="stylesheet" href="views/css/loginPage.css">
<script src="views/js/addJury.js" defer></script>

<?php
$headers=ob_get_clean() ;
ob_start() ?>
<div class="flex-center">
    <div class="wrapper">
        <div class="form_wrapper">
            <div class="form_content">
                <h1>Créer un jury</h1>
                <form id="jury_form" class="custom_form" >
                    <div class="form_label">
                      <label for="id_planning">Planning</label>
                      <select id="id_planning" name="id_planning" form="jury_form" required>
                          <option value="">--Choisir un planning--</option>
                      </select>
                    </div>
                    <div class="form_label">
                      <label for="id_salle">Planning</label>
                      <select id="id_salle" name="id_salle" form="jury_form" required>
                          <option value="">--Choisir une salle--</option>
                      </select>
                    </div>
                    <div class="form_label">
                      <label for="id_utilisateur1">Planning</label>
                      <select id="id_utilisateur1" name="id_utilisateur1" form="jury_form" required>
                          <option value="">--Choisir un professeur--</option>
                      </select>
                    </div>
                    <div class="form_label">
                      <label for="id_utilisateur2">Planning</label>
                      <select id="id_utilisateur2" name="id_utilisateur2" form="jury_form" required>
                          <option value="">--Choisir un professeur--</option>
                      </select>
                    </div>
                    <div>
                      <label for="date_deb">Date début</label>
                      <input type="datetime-local" id="date_deb" name="date_deb" min="08:00" max="18:00" form="jury_form" required>
                    </div>

                    <div>
                      <label for="date_fin">Date fin</label>
                      <input type="datetime-local" id="date_fin" name="date_fin" form="jury_form" required>
                    </div>

                    <span id class="errorMsg hide"></span>

                    <input type="submit" value="Valider et créer le jury" title="Valider et créer le jury"/>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
$title = "Ajouter un jury";
$content = ob_get_clean();
require_once('views/templates/main.php')
?>
