<?php ob_start();?>
<link rel="stylesheet" href="views/css/homePage.css">

<?php $headers=ob_get_clean() ?>

<?php ob_start() ?>
<div id="homepage" class="flex-center">
    <div class="center-box">
        <h1>Formations</h1>
        <div class="split-2">
            <div class="home_part">
                <h2>Cyberdata</h2>
                <div class="justify-text">
                    <img src="views/img/cyberdata.jpeg"/>
                    Combine des compétences venant de l'informatique, des statistiques, de l'intelligence artificielle, des méthodes de chiffrement et de la cybersécurité le tout dans une orientation "big data" et calcul intensif. Maîtrose un double enjeu : séucriser les données et leur traitement et apporter la puissance de la sciende des données à la cybersécurité.
                </div>
            </div>
            <div class="divider"></div>
            <div class="home_part">
                <h2>Cyberlog</h2>
                <div class="justify-text"">
                    <img src="views/img/cyberlog.jpeg"/>
                    Un ingénieur informaticien capable de comprendre et prendre en compte les risques lés aux vulnéraiblités logicielles et de proposer et mettre en place des systèmes réopdnant à de fortes exigences en terme de sécurité et de sûreté. Il apporte la sécurité par conception.
                </div>
            </div>
        </div>
    </div>
</div>

<?php $content=ob_get_clean() ?>

<?php

$title="Stages - Accueil";
require_once ('views/templates/main.php');
?>
