<?php

$part_url = HTTP_URL . "dashboard&part=";

$page = "profile";
$pages = ["profile" => ["url" => $part_url . 'profile', 'libelle' => "Mes informations"]];

$user = $_SESSION["user"];
if ((bool)$user->etudiant === true) {
    $pages["documents"] = ["url" => $part_url . 'documents', 'libelle' => "Mes documents"];
    $pages["offres"] = ["url" => HTTP_URL . 'offres', 'libelle' => "Voir les offres de stages"];
}
if (strtolower($user->type_utilisateur) === "entreprise") {
    $pages["documents"] = ["url" => $part_url . 'documents', 'libelle' => "Mes offres"];
    //$pages["cv"] = ["url" => HTTP_URL . 'cvtheque', 'libelle' => "Cvthèque"];
    $pages["liste-etud"] = ["url" => HTTP_URL . 'liste-etud', 'libelle' => "CVthèque"];
}

if(strtolower($user->type_utilisateur)==="administrateur"){
    $pages["admin_users"] = ["url" => HTTP_URL.'admin_users', 'libelle' => "Gérer les utilisateurs"];
    $pages["liste-etud"] = ["url" => HTTP_URL . 'liste-etud', 'libelle' => "CVthèque"];
    $pages["create-salle"] = ["url" => HTTP_URL . 'create-salle', 'libelle' => "Création de Salle"];
    $pages["offres"] = ["url" => HTTP_URL . 'offres', 'libelle' => "Voir les offres de stages"];  
    $pages["add-offre"] = ["url" => $part_url . 'add-offre', 'libelle' => "Ajouter une offre"];
    $pages["liste_planning"] = ["url" => $part_url . 'liste_planning', 'libelle' => "Plannings"];

}

if (isset($_GET["part"])) {
    $part = htmlspecialchars($_GET["part"]);
    if (array_key_exists($part, $pages)) {
        $page = $part;
    }
}

function setActive($part){
    global $page;
    if ($part === $page) {
        return "class='active'";
    }
    return null;
}
?>

<?php ob_start(); ?>

<?php if(file_exists("views/css/partials/dashboard/".$page.".css")){ ?>
    <link rel="stylesheet" href="<?= "views/css/partials/dashboard/".$page . ".css" ?>">
<?php } ?>
<?php if(file_exists("views/js/partials/dashboard/".$page.".js")){ ?>
    <script src="views/js/partials/dashboard/<?= $page . ".js" ?>" defer></script>
<?php } ?>
<?php $newheaders = ob_get_clean() ?>

<?php ob_start(); ?>
<h1>Tableau de bord</h1>
<?php foreach ($pages as $p => $values) { ?>
    <a href="<?= $values["url"] ?>" <?= setActive($p) ?>><?= $values["libelle"] ?></a>
<?php } ?>
<?php $dashboard_choices = ob_get_clean() ?>

<?php ob_start() ?>
Ceci est le info_content
<?php $info_content = ob_get_clean() ?>


<?php ob_start() ?>
<?php
if(file_exists("views/pages/partials/dashboard/" . $page . ".php")){
    require_once "views/pages/partials/dashboard/" . $page . ".php";
}else{
    echo "La page ".htmlspecialchars($page). " n'existe pas";
}
$right_part = ob_get_clean() ?>

<?php
$title = "Dashboard";
require_once('views/templates/dashboard.php');
?>
