<?php ob_start(); ?>
<link rel="stylesheet" href="views/css/dashboard.css">
<?= $newheaders ?>

<?php $headers = ob_get_clean() ?>

<?php ob_start() ?>

<div class="dash_container">
    <div class="dashboard-left-part">
        <div class="dashboard_choices shadow-box">
            <?= $dashboard_choices ?>
        </div>

        <div class="dashboard-infos shadow-box">
            <div class="dashboard-infos-title">
                <h1>Informations utiles</h1>
            </div>
            <div class="dashboard-infos-content">
                <p><?= $info_content ?></p>
            </div>

        </div>

    </div>
    <div class="dashboard-right-part shadow-box">
        <?= $right_part ?>
    </div>


</div>

<?php $content = ob_get_clean() ?>

<?php
require_once('views/templates/main.php');
?>
