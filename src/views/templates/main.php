<!DOCTYPE html>

<html lang="fr">
    <?php require_once('views/headers.php') ?>
    <?php
    if(isset($headers)){
        echo $headers;
    } ?>
<body>
<div class="container">
    <?php require_once('views/navbar.php') ;?>

    <div id="container_main_content">
        <?php
        if(isset($content)){
            echo $content;
        } ?>
    </div>
    <?php require_once('views/footer.php'); ?>
</div>
</body>

</html>