<footer class="footer">
    <div class="navbar-footer-content footer-content">
        <div class="footer-left">
            <p class="footer-copyright">© 2021 - Superbrow Dev</p>
        </div>

        <div class="footer-right">
            <a href="https://www-ensibs.univ-ubs.fr/fr/suivez-nous/facebook.html"><img src="views/img/logo-facebook.svg"></a>
            <a href="https://www-ensibs.univ-ubs.fr/fr/suivez-nous/instagram.html"><img src="views/img/logo-instagram.svg"></a>
            <a href="https://www-ensibs.univ-ubs.fr/fr/suivez-nous/linkedin.html"><img src="views/img/logo-linkedin.svg"></a>
            <a href="https://www-ensibs.univ-ubs.fr/fr/suivez-nous/twitter.html"><img src="views/img/logo-twitter.svg"></a>
            <a href="https://www-ensibs.univ-ubs.fr/fr/suivez-nous/youtube.html"><img src="views/img/logo-youtube.svg"></a>
        </div>
    </div>
</footer>
