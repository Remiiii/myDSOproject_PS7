$(function () {
    let student_div=$("#student_div");
    let offres_div=$("#offres_container");
    if(student_div.length){
        var url = "/api/documents/student"

        $.ajax({
            url: url,
            type: 'GET',
        }).done(data => {
            data = data["data"];
            for (let i = 0; i < data.length; i++) {
                fileExists(data[i]["id_type_document"], true);
            }
            showForm();
        })

        /**
         * show or hide divs if file exists or is added
         * @param id_type_doc
         * @param exist
         */
        function fileExists(id_type_doc, exist) {
            let exist_el = $(`#existing_files tr[data-type=${id_type_doc}]`);
            let not_exist_el = $(`#document_form tr[data-type=${id_type_doc}]`)

            let label = $(not_exist_el).find('.label_file_upload');
            if (exist === true) { //ajouter doc
                exist_el.removeClass('hide');
                not_exist_el.addClass('hide');
                $(`#existing_files`).removeClass('hide');
            } else {
                label.text(not_exist_el.data("label"));
                exist_el.addClass('hide');
                not_exist_el.removeClass('hide');
                showExistingDocuments();
                $("#document_form").removeClass('hide')//show form
            }
        }

        $('.supprimer').on('click', function (event) {
            let parent_el = getParent($(this));

            let id_type = parent_el.data("type")

            $.ajax({
                url: url + '/' + id_type,
                type: 'DELETE',
            }).done(res => {
                if (res["status"] === true) {
                    fileExists(id_type, false);
                    showError(null,false);
                }
            })
        })

        $("form#document_form").submit(function (e) {
            e.preventDefault();

            let inputs=$(`form#document_form tr:not(".hide")`);
            let not_hidden=inputs.find(`input[type=file]`);

            let data=new FormData();
            for (let i = 0; i < not_hidden.length; i++) {
                let item=$(not_hidden[i])
                let name=item.attr("name");
                let file=item.get(0).files[0];
                if(file!==undefined)data.append(name, file);
            }
            
            $.ajax({
                url: url + '/upload',
                type: 'POST',
                contentType: false,
                processData: false,
                data: data,
            }).done(data => {
                for (const type in data) {
                    let status = data[type]['status'];

                    if (status === true) {
                        fileExists(type, true);
                    }
                }
                this.reset();
                resetLabels();
                showExistingDocuments();
            }).fail(err=>{
                err=err.responseJSON;
                
                for (const type in err) {
                    let status = err[type].status;
                    if(status===false){
                        let label=$(`.file_display[data-type=${type}]`);
                        let erreurMsg=label.data("label")+": "+err[type].message;
                        showError(erreurMsg,true);
                    }
                }
            })
        })

        function showError(message,show){
            if(show){
                $(`.errorMsg`).text(message).removeClass('hide');
            }else{
                $(`.errorMsg`).text('').addClass("hide");
            }
        }

        function getParent(context) {
            return context.closest(`.file_display`);
        }

        $('.cancel').on("click", function (e) {
            e.preventDefault();
            resetLabelByChild($(this))
            $(this).addClass('hide');
        })

        function resetLabelByChild(child) {
            let parent = $(getParent(child))
            let label = parent.find('.label_file_upload');
            let inputFile = parent.find(`input[type=file]`)
            let cancel_button = parent.find(`.cancel`);
            cancel_button.addClass('hide');
            label.text(parent.data("label"))
            inputFile.val("");
        }

        function resetLabels() {
            let inputs = $(`.label_file_upload`);
            for (let i = 0; i < inputs.length; i++) {
                resetLabelByChild(inputs[i])
            }
            showForm();
        }

        function showExistingDocuments(){
            let visible_els = $('#existing_files .file_display:visible');
            if (visible_els.length <= 0) $(`#existing_files`).addClass('hide');
        }

        function showForm() {
            let not_hidden = $('#document_form .file_display:visible');
            if (not_hidden.length <= 0) $(`#document_form`).addClass('hide');
        }

        $("form#document_form input[type=file]").on("change", function (e) {
            e.preventDefault();

            let parent = getParent($(this));
            let label = parent.find('.label_file_upload');

            let filename = e.target.files[0].name;

            label.text(filename);

            let input = parent.find(`input.cancel`);
            input.removeClass('hide')
        });

        $(`.file_display .voir`).on('click',function (e){
            e.preventDefault();
            let parent=getParent($(this));
            let id_type=parent.data("type");
            let w=window.open(url+"/download/"+id_type,"New page");
        })
    }


})