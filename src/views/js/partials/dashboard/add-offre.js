var entreprise_select = document.getElementById("entreprise-select");   // On récupère la liste déroulante
var files_lem = document.getElementById("offre");

$(function (){
    var liste_entreprise = [];                                              // Liste des entreprises
    var liste_entreprise_id = []                                           // Liste des id des entreprises
    var result;                                                             // Résultat de la requete

// Requete pour obtenir la liste des étudiants
    ajax(`/api/user/type/Entreprise`, "GET", undefined, answer => affiche(answer));

// On ajoute les liens sur le boutons
    let btn = document.getElementById("send");
    btn.addEventListener("click",uploadDoc);

// Fonction qui appelle getEntreprise apres le resultat de la requete
    function affiche(answer){
        if(answer.status === true){
            result = answer.data;
            getEntreprise();
        }
    }

// Fonction qui met à jour la liste déroulante des entreprises.
    function getEntreprise(){
        for(let i = 0; i< result.length; i++){
            liste_entreprise.push(result[i].nom);
            liste_entreprise_id.push(result[i].id_utilisateur);
        }
        for(let i = 0; i< liste_entreprise.length; i++){
            let option = document.createElement('option');
            option.setAttribute("value",liste_entreprise[i]);
            option.innerHTML = liste_entreprise[i];
            entreprise_select.appendChild(option);
        }
    }

// Fonction qui upload l'offre de l'entreprise
    function uploadDoc(){
        let indice = entreprise_select.selectedIndex - 1;                   // On récupère l'indice de la liste déroulante
        let file_size = files_lem.value.length;      // On récupère le nombre de fichier fourni
        let id = liste_entreprise_id[indice];                               // On récupère l'id de l'entreprise selectionnée
        let data = new FormData();

        if(indice >= 0){            // On vérifie qu'une entreprise est bien selectionnée
            if(file_size >= 0){     // On vérifie qu'il y a au moins un fichier sélectionné
                let files = files_lem.files;
                if(files.length !== 0){
                    for(let i = 0; i < files.length; i++){
                        data.append('file[]',files[i]);
                    }
                    ajax(`/api/documents/upload/${id}`, "POST", data, answer => uploadDone(answer));
                }
            }else{
                alert("Pas de fichier sélectionné !");
            }
        }else{
            alert("Pas d'entreprise sélectionnée !")
        }
    }

// Fonction qui est appelée après le résultat de la requete AJAX
    function uploadDone(answer){
        files_lem.value="";
        entreprise_select.selectedIndex=0
        alert(answer.insertedCount+" fichiers ajoutés avec succès");
    }

})
