$(function (){
    $("#logout_button").on("click",function (event){
        event.preventDefault();

        $.get("/api/auth/logout",function (data){
            window.location="?page=login_form";
        })
    })
})