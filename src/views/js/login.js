$(function (){
    $("#login_form").submit(function (e){
        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = e.currentTarget.action;

        //do your own request an handle the results
        $.ajax({
            url: actionurl,
            type: 'POST',
            contentType: 'application/json;',
            data: $("#login_form").serialize(),
            success: function(data) {
                if(data.status===true){
                    window.location='?page=dashboard';
                }else{
                    $("input[type='password']").val('');
                    $(".errorMsg").text(data.message).removeClass('hide');
                }
            },
        })
    })
})