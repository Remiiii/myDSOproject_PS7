var carousel = document.getElementById("myCarousel");       // On récupère le carousel
var id = carousel.getAttribute("data-id");                  // On récupère l'id de l'étudiant que l'on souhaite afficher
var url_api = carousel.getAttribute("data-url");            // On récupère l'url de l'api

var liste_etudiant = [];                                    // Liste des id
var liste_etudiant_nom = [];                                // Liste des noms 
var liste_etudiant_prenom = [];                             // Liste des prénoms
var indice = 0;                                             // Réprésente l'indice dans le tableau des étudiants

var result;                                                 // Résultat de la requete du tableau des étudiants

// Requete pour obtenir la liste des étudiants
ajax(`/api/documents/getStudents`, "GET", undefined, answer => refreshTabUser(answer));

var btn_retour = document.getElementById("button-liste");   // On ajoute la redirection lors du clique du bouton
btn_retour.addEventListener("click",retourListe);

var btn_suiv = document.getElementById("button-suiv");   // On ajoute la redirection lors du clique du bouton
btn_suiv.addEventListener("click",suivant);

var btn_prec = document.getElementById("button-prec");   // On ajoute la redirection lors du clique du bouton
btn_prec.addEventListener("click",precedent);

// Fonction qui appelle changeSrc apres le resultat de la requete
function affiche(answer){
    if(answer.status === true){
        var result = answer.data;
        
        changeSrc(answer.data);        
    }
}

// Méthode qui met à jour les documents du carousel en fonction de l'id donné en paramètre GET
function changeSrc(answer){
    // On récupère les nouveaux id des documents
    // Et on change les sources des documents du carousel
    
    /*
    if(answer.data[0].id_document !== undefined){
        let id_cv = answer.data[0].id_document;
        cv.setAttribute("src",url_api+id_cv);
    }
    if(answer.data[1].id_document !== undefined){
        let id_lm = answer.data[1].id_document;
        lm.setAttribute("src",url_api+id_lm);
    }
    if(answer.data[2].id_document !== undefined){
        let id_lr = answer.data[2].id_document;
        lr.setAttribute("src",url_api+id_lr);
    }
    */
    
    var carousel = $(".carousel-inner");
    carousel.empty();

    // On ajoute tous les documents dynamiquement
    answer.map(doc => {
        // 
        let item = document.createElement('div');
        $(item).addClass("item");
        let embed = document.createElement('embed');
        embed.setAttribute("class","document");
        embed.setAttribute("height","1043");
        embed.setAttribute("width","700");
        embed.setAttribute("src",url_api+doc.id_document);    
        
        item.append(embed);
        carousel.append(item);
        
    })
    // On ajoute la classe active au premier document
    $(".item").first().addClass("active");
}

function suivant(){ // Fonction qui gère d'afficher l'élèves suivant de celui actuel dans la liste des étudiants.
    if(indice < (liste_etudiant.length - 1)){
        indice = indice + 1;  
        window.location=`?page=cvtheque&id=${liste_etudiant[indice]}`;      
    }
    /*
    id = liste_etudiant[indice];
    ajax(`/api/documents/${id}`, "GET", undefined, answer => affiche(answer));
    
    
    showNameStudent();
    */
}

function precedent(){ // Fonction qui gère d'afficher l'élèves précédent de celui actuel dans la liste des étudiants.
    if(indice > 0){
        indice = indice - 1;    
        window.location=`?page=cvtheque&id=${liste_etudiant[indice]}`;           
    }
    /*
    id = liste_etudiant[indice];    
    ajax(`/api/documents/${id}`, "GET", undefined, answer => affiche(answer));
     
    
    showNameStudent();
    */
    
}

// Permet de récuperer la liste des étudiants
function refreshTabUser(answer){
    if(answer.status === true){
        result = answer.data;
        refreshDataboard();        
    }
}

// Permet de récuperer la liste des étudiants
function refreshDataboard(){
    for (let i = 0; i < result.length; i++) {
        if(liste_etudiant.indexOf(result[i].id_utilisateur) < 0){
            liste_etudiant.push(result[i].id_utilisateur);
            liste_etudiant_nom.push(result[i].nom_utilisateur);
            liste_etudiant_prenom.push(result[i].prenom_utilisateur);
        }
    }

    
    
    

    

    if(id == ""){   // Si pas d'id on prend le premier de la liste des étudiants
        id = liste_etudiant[0];
        indice = 0;
        
        
    }else{  // Sinon on récupère l'indice dans le tableau de l'id donnée
        indice = liste_etudiant.indexOf(id);
        
        
        
    }

    // Requete pour obtenir les documents d'un étudiant
    ajax(`/api/documents/${id}`, "GET", undefined, answer => affiche(answer));
    
    showNameStudent();
}

// Met à jour le nom dans le titre
function showNameStudent(){
    document.getElementById("titre").innerHTML = `CVtheque de ${liste_etudiant_prenom[indice]} ${liste_etudiant_nom[indice]}`
}

// Permet un retour à la liste avec tous les étudiants
function retourListe(){
    window.location='?page=liste-etud';
}

/*
FAIT - Recupérer la liste des etuds

FAIT - Récupérer etudiant actuel avec data_id qui sera dans le mycarousel$

FAIT - Récupère les id des docs associé à partir de l'id etud

FAIT - Changer le src des embed avec les nouveaux id des doc de l'etud

FAIT - Afficher nom et prenom

FAIT - Remettre le premier document en actif 

Changer couleur des boutons

*/

