var offre = document.getElementById("offre");               // On récupère l'offre de l'entreprise
var titre = document.getElementById("titre");               // On récupère le titre
var container = document.getElementById("container");

var liste_entreprise = [];                                    // Liste des id
var liste_entreprise_nom = [];                                // Liste des noms 
var indice = 0;                                               // Réprésente l'indice dans le tableau des étudiants

var url_api = container.getAttribute("data-url");           // On récupère l'url de l'api

var btn_suiv = document.getElementById("button-suiv");   // On ajoute la redirection lors du clique du bouton
btn_suiv.addEventListener("click",suivant);

var btn_prec = document.getElementById("button-prec");   // On ajoute la redirection lors du clique du bouton
btn_prec.addEventListener("click",precedent);

// Requete pour obtenir les offres
ajax(`/api/documents/offres`, "GET", undefined, answer => affiche(answer));

function affiche(answer){
    if(answer.status === true){
        var result = answer.data;
        getData(answer.data);       
    }else{
        let container = document.getElementById("container");
        let empty = document.getElementById("empty_list");
        empty.style.display = "block";
        container.style.display = "none";
    }
}

// Fonction qui récupère les données après la requete
function getData(data){
    
    for(let i = 0; i < data.length; i++){
        liste_entreprise[i] = data[i].id_document;
        liste_entreprise_nom[i] = data[i].nom_utilisateur;
    }
    
    
    changeSrc();
}

// Méthode qui met à jour les documents du carousel en fonction de l'id donné en paramètre GET
function changeSrc(){
    offre.setAttribute("src",url_api+liste_entreprise[indice]);   
    
    titre.innerHTML = 'Offre de ' + liste_entreprise_nom[indice];  
    
}

function suivant(){ // Fonction qui gère d'afficher l'élèves suivant de celui actuel dans la liste des étudiants.
    if(indice < (liste_entreprise.length - 1)){
        indice = indice + 1;  
        changeSrc();      
    }
}

function precedent(){ // Fonction qui gère d'afficher l'élèves précédent de celui actuel dans la liste des étudiants.
    if(indice > 0){
        indice = indice - 1;    
        changeSrc();        
    }
}