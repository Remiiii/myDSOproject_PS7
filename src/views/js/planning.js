var planning;

/**
 * Vérifie que l'ID est un nombre, et que le planning correspondant existe en BDD
 * @returns {boolean} true si l'ID est valide, false s'il ne l'est pas
 */
 async function checkIDValidity(id) {
    // doit être un entier
    if (Number.isInteger(Number(id))) {
        let res = await ajaxSync("/api/planning/" + id, "GET", undefined);
        if (res.status) {planning = res.data[0];}
        return res.status;
    } else {
        console.error(id)
        return false;
    }
}

/**
 * Écrit le message d'erreur passé en paramètres au bon endroit du HTML,
 * et dé-cache la div du message d'erreur
 * @param {String} error
 */
function displayError(error) {
    document.getElementById("errormessage").innerHTML = error;
    document.getElementById("error").classList.remove("hidden");
}

/**
 * 
 * @param {String} id 
 * @param {Date} dateDeb 
 * @param {Object[]} jurys 
 * @param {Object[]} entretiens 
 * @returns HTMLElement prêt à être append dans la page
 */
function bulleFactory(id, dateDeb, dateFin, jurys, entretiens) {
    // création d'un HTMLElement div
    let bulle = document.createElement('div');
    // attributs de la bulle
    bulle.setAttribute("data-show-hover", id);
    bulle.classList.add("hidden", "bubble");
    
    // contenu de la bulle
        // heure du créneau
    let heure = document.createElement('span');
    heure.classList.add("bulleHeure");
    heure.innerHTML = dateDeb.getHours() + "h" + dateDeb.getMinutes() + " - " + dateFin.getHours() + "h" + dateFin.getMinutes();
    bulle.appendChild(heure);
        // liste jurys/entretiens
    let table = document.createElement('table');
    jurys.forEach(jury => {
        let ligne = document.createElement('tr');

        let colJury = document.createElement('td');
        colJury.innerHTML = jury.nom_salle + " > " + jury.nom_utilisateur1 + " & " + jury.nom_utilisateur2;
        ligne.appendChild(colJury);

        let colEntretien = document.createElement('td');
        let entretien = entretiens.filter(entretien => entretien.id_jury == jury.id_jury);
        if (entretien.length > 0) {
            // il y a un entretien pour ce jury à cette heure
            let etudiant = document.createElement('span');
            etudiant.innerHTML = "> " + entretien[0].nom_etudiant;
            colEntretien.appendChild(etudiant);
            // ajouter l'option pour supprimer l'entretien
            let supprimerEntretien = document.createElement('span');
            supprimerEntretien.classList.add("supprimerentretien");
            supprimerEntretien.innerHTML = "x";
            supprimerEntretien.onclick = function(ev) {
                ajax("/api/planning/entretiens/" + entretien[0].id_entretien, "DELETE", undefined, (res) => {
                    if (res.status) {
                        // mettre à jour le tableau
                        console.log(this);
                        window.location.reload();
                    };
                });
            };
            colEntretien.appendChild(supprimerEntretien);
        } else {
            // il n'y a pas d'entretien pour ce jury à cette heure
            colEntretien.innerHTML = ">";
            // ajouter l'option pour créer un entretien
            let ajouterEntretien = document.createElement('span');
            ajouterEntretien.classList.add("ajouterentretien");
            ajouterEntretien.innerHTML = "+";
            ajouterEntretien.setAttribute("data-creneau", dateDeb);
            ajouterEntretien.onclick = function(ev) {
                // modal de sélection d'étudiant
                // trouver tous les étudiants sans entretien

                // créer la liste de ces étudiants
                
            }
            colEntretien.appendChild(ajouterEntretien);
        }
        ligne.appendChild(colEntretien);

        table.appendChild(ligne);
    });
    bulle.appendChild(table);

    return bulle;
}

/**
 * Gère le rendu du planning
 * @param {Object[]} entretiens 
 * @param {Object[]} jurys 
 */
function displayPlanning(entretiens, jurys) {
    let HEURE_DEB = 8;//heure de debut de journee
    let div_planning = document.getElementById("planning");
    let thead = document.createElement('thead');
    let tbody = document.createElement('tbody');
    let tr = document.createElement('tr');
    planning.date_deb = new Date(planning.date_deb);
    planning.date_fin = new Date(planning.date_fin);
    let index_jour = new Date(planning.date_deb);
    let nb_crenaux = 10/(planning.duree_creneau/60); //On dit qu y 10h de rdv 8h->18h;
    let heure = new Date(index_jour.getFullYear(),index_jour.getMonth(),index_jour.getDate(),HEURE_DEB);
    let nb_jour;
    let th;
    let td;
    // On enlève le hidden de la div
    div_planning.classList.remove("hidden");
    //On ajoute l entete
    div_planning.appendChild(thead);
    thead.appendChild(tr);
    //Calcule du nombre de jour de diff
    if (planning.date_fin.getTime() - planning.date_deb.getTime() < 1000*3600*24) {
        //date_fin meme jour que date_deb
        nb_jour = 1;
    }
    else {
        //le 0.0001 est insignifiant et permet d arriver au résultat attendu si les nombre tombent ronds
        //ex : 1/1/2000 - 1/1/2000 = 0 + 0.0001 = 1 (avec le Math.ceil)
        nb_jour = Math.ceil((planning.date_fin.getTime() - planning.date_deb.getTime())/(1000*3600*24)+0.0001);
    }
    //creation des colonnes
    //commence a 0 pour prendre en compte la double entree du tableau
    for(let num_colonne = 0; num_colonne < nb_jour+1; num_colonne++) {
        if(num_colonne != 0) {
            //On enleve l heure
            let d = new Date(index_jour.getFullYear(),index_jour.getMonth(),index_jour.getDate());
            th = document.createElement('th');
            //On affiche bien la date
            th.innerHTML=d.toLocaleString('fr-FR',{weekday: 'long',year: 'numeric',month: 'long',day: 'numeric'});
            tr.appendChild(th);
            index_jour.setDate(index_jour.getDate()+1);
        }
        else {
            th = document.createElement('th');
            tr.appendChild(th);
        }
    }
    //On ajoute le corps
    div_planning.appendChild(tbody);

    //creation des lignes
    for(let num_ligne = 0; num_ligne < nb_crenaux+1; num_ligne++) {
        tr = document.createElement('tr');
        th = document.createElement('th');
        //affichage de l heure
        th.innerHTML = heure.toLocaleString('fr-FR', {hour: 'numeric', minute: 'numeric'} );//heure.getHours()+"h"+heure.getMinutes();
        tr.appendChild(th);
        // incrément de l heure
        heure.setMinutes(heure.getMinutes() + parseInt(planning.duree_creneau));
        //On genere les lignes
        for(let i=0; i < nb_jour; i++) {
            td = document.createElement('td');
            //on ajoute les ids sous la forme <jj>-<mm>-<aaaa>:<hh>-<mm>
            // i est le numéro de jour, num_ligne est le numéro de créneau
            let creneau = new Date(planning.date_deb);
            creneau.setHours(8);
            creneau.setDate(creneau.getDate() + i);
            creneau.setMinutes(creneau.getMinutes() + planning.duree_creneau * num_ligne);
            td.id = creneau.getDay() + "-" + creneau.getMonth() + "-" + creneau.getFullYear()
                + ":" + creneau.getHours() + "-" + creneau.getMinutes().toString();

            // contenu de la case : <nombre d'entretiens se déroulant à ce créneau> / <nombre de jurys travaillant à ce créneau>
            // jurys dispos à ce créneau
            let jurysCreneau = [];
            if (jurys != undefined) { jurysCreneau = jurys.filter(jury => jury.date_deb <= creneau && jury.date_fin >= creneau); }
            // entretiens se déroulant à ce créneau
            let entretiensCreneau = [];
            if (entretiens != undefined) { entretiensCreneau = entretiens.filter(entretien => entretien.date_deb.toString() == creneau.toString()); }
            // on écrit le décompte de chaque dans la case
            td.innerHTML = entretiensCreneau.length + "/" + jurysCreneau.length;
            // colorer la case en fonction des valeurs
            if (jurysCreneau.length == 0 && entretiensCreneau.length == 0) { td.classList.add("nobody"); }
            else if (jurysCreneau.length > entretiensCreneau.length) { td.classList.add("notfull"); }
            else { td.classList.add("full"); }

            // ajout de la bulle
            let finCreneau = new Date(creneau);
            finCreneau.setMinutes(finCreneau.getMinutes() + planning.duree_creneau);
            let bulle = bulleFactory(td.id, creneau, finCreneau, jurysCreneau, entretiensCreneau);
            div_planning.appendChild(bulle);

            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
}

/**
 * Certains éléments HTML ont la propriété data-show-hover="<id>"
 * Quand la souris est en hover sur cet élément ou sur l'élément <id>,
 * alors l'élément est affiché, sinon il est caché
 */
function setupShowOnHover() {
    // récupérer la liste des éléments qui ont l'attribut "data-show-hover"
    document.querySelectorAll("[data-show-hover]")
    .forEach(element => {
        // récupérer l'élément qui doit être survolé pour afficher le premier élément
        let caller = document.getElementById(element.getAttribute("data-show-hover"));

        // au survol du caller, on dé-cache
        caller.addEventListener("mouseover", (_ev) => {
            element.classList.remove("hidden");
        });
        // à la sortie du caller, on re-cache
        caller.addEventListener("mouseout", (_ev) => {
            element.classList.add("hidden");
        });

        // au survol de l'élément, on dé-cache
        element.addEventListener("mouseover", (_ev) => {
            element.classList.remove("hidden");
        });
        // à la sortie de l'élément, on re-cache
        element.addEventListener("mouseout", (_ev) => {
            element.classList.add("hidden");
        });

        // positionner la bulle en fonction du caller
        let callerBox = caller.getBoundingClientRect();
        let top = callerBox.y + callerBox.height / 2;
        let left = callerBox.x + callerBox.width / 2;
        element.style.top = top + "px";
        element.style.left = left + "px";
    });
}

/**
 * Récupère la liste des entretiens associés au planning id
 * @param {Number} idPlanning doit être un entier
 * @returns {Boolean, Object[]} objet réponse de la requête {status: Boolean, data: Array}
 */
function loadEntretiens(idPlanning) {
    return ajaxSync("/api/planning/" + idPlanning + "/entretiens/", "GET", undefined);
}

/**
 * Récupère la liste des jurys associés au planning id
 * @param {Number} idPlanning doit être un entier
 * @returns {Boolean, Object[]} objet réponse de la requête {status: Boolean, data: Array}
 */
function loadJurys(idPlanning) {
    return ajaxSync("/api/planning/" + idPlanning + "/jurys/", "GET", undefined);
}

/**
 * Fonction appelée au chargement de la page
 */
async function main() {
    // récupérer l'id du planning là où PHP l'a placé
    idPlanning = document.getElementById("title").getAttribute("data-id");

    // le checkIDValidity récupère le planning de la BDD
    if (await checkIDValidity(idPlanning)) {
        idPlanning = Number(idPlanning);

        // récupérer les jurys et entretiens
        let entretiens = (await loadEntretiens(idPlanning)).data;
        if (entretiens != undefined) {
            entretiens.forEach(entretien => {
            entretien.date_deb = new Date(entretien.date_deb);
            entretien.date_fin = new Date(entretien.date_fin);
        }); }
        let jurys = (await loadJurys(idPlanning)).data;
        if (jurys != undefined) {
            jurys.forEach(jury => {
            jury.date_deb = new Date(jury.date_deb);
            jury.date_fin = new Date(jury.date_fin);
        }); }

        // afficher planning
        displayPlanning(entretiens, jurys);

        // titre du planning
        document.getElementById("title").innerHTML = "Planning #" + idPlanning + " \"" + planning.nom + "\" - "
            + planning.type_planning + "s du " + planning.date_deb.toLocaleDateString() + " au "
            + planning.date_fin.toLocaleDateString();

        // appliquer les attributs data-show-hover="id"
        setupShowOnHover();
    } else {
        displayError("L'ID de planning \"" + idPlanning + "\" est invalide.");
    }
}

main();
