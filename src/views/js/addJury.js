

$(function (){
  var opt_planning = document.getElementById('id_planning');
  var opt_salle = document.getElementById('id_salle');
  var opt_utilisateur1 = document.getElementById('id_utilisateur1');
  var opt_utilisateur2 = document.getElementById('id_utilisateur2');
  var date_deb = document.getElementById('date_deb');
  var date_fin = document.getElementById('date_fin');
  var custom_msg = $('.errorMsg');
  var dateNow = new Date();
  var option;

  //affiche les plannings
  ajax("api/planning/", "GET", undefined, result => {
    result.forEach(function (e){
      option = document.createElement('option');
      option.value = e.id_planning;
      option.innerHTML = e.nom;
      opt_planning.appendChild(option);
    })
  });

  //affiche les salles
  ajax("/api/salle/", "GET", undefined, result => {
    result.forEach(function (e){
      option = document.createElement('option');
      option.value = e.id_salle;
      option.innerHTML = e.nom;
      opt_salle.appendChild(option);
    })
  });
  //affiche les utilisateurs
  ajax("/api/user/type/professeur", "GET", undefined, result => {

    result.data.forEach(function (e){
      option = document.createElement('option');
      option.value = e.id_utilisateur;
      option.textContent = e.nom+" "+e.prenom;
      opt_utilisateur1.appendChild(option);
      option = document.createElement('option');
      option.value = e.id_utilisateur;
      option.textContent = e.nom+" "+e.prenom;
      opt_utilisateur2.appendChild(option);
    })
  });

  //execution onclick
  $("#jury_form").on("submit",function (e){
    e.preventDefault();

    /*
    let date_deb = new Date(time_heure_deb.value+'T'+time_date_deb.value);
    let date_fin = new Date(time_heure_fin.value+'T'+time_date_fin.value);
    let json;
*/
    if(opt_utilisateur1.selectedIndex === opt_utilisateur2.selectedIndex){
      $(opt_utilisateur2).css('borderColor', 'red');
      custom_msg.text("Les professeurs doivent être différents");
      custom_msg.removeClass('hide');
      return false;
    }
    else{
      $(opt_utilisateur2).css('borderColor', '#C4C4C4');
      custom_msg.text("");
      custom_msg.addClass("hide");

      //envoie des donnees
      ajax("/api/planning_create/jury", "POST", $('form#jury_form').serialize(), result => {
        custom_msg.text(result.message);
        custom_msg.removeClass('hide');
      });
    }
  })
})
