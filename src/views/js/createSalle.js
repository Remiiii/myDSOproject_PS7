var form = document.getElementById("form");	// On récupère le bouton

var name;									// Nom de la salle

var date_deb;								// Date de début de disponibilité, format datetime-local
var date_fin;								// Date de fin de disponibilité, format datetime-local

// On ajoute le click sur le bouton
$(form).on("submit",function (e){
	e.preventDefault();
	uploadData();
})



// Fonction qui envoie la requête pour créer la salle
function uploadData(){
	
	let data = $(form).serialize();

	// Requete pour obtenir la liste des étudiants
	ajax(`/api/planning_create/salle`, "POST", data, answer => result(answer));

	vide_form();
}

// Fonction qui est appelée après la requête
function result(answer){
	console.log(answer);
}

//Fonction qui vide le formulaire
function vide_form(){
	document.getElementById("name").value = "";
}
