async function ajaxSync(url, type, data)
{
    return await ( await fetch(url, { method: type, body: data })).json();
}

function ajax(url, type, data, callback)
{
    fetch(url, { method: type, body: data })
    .then(r => r.json()
        .then(r => callback(r))
    );
}
