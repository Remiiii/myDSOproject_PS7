// Requete pour obtenir les documents d'un utilisateur
ajax(`/api/documents/getStudents`, "GET", undefined, answer => affiche(answer));

var tableau = document.getElementById("donnee");    // On récupère le tableau
var result;                                         // Résultat de la requete 

var liste_etudiant_id = [];                         // Liste des id
var liste_etudiant_nom = [];                        // Liste des noms 
var liste_etudiant_prenom = [];                     // Liste des prénoms
var liste_etudiant_filiere = [];                    // Liste des filières

// Fonction qui appelle refreshDataboard apres le resultat de la requete
// Si pas d'étudiant alors on cache la cvtheque et on affiche la div empty_list
function affiche(answer){
    if(answer.status === true){
        result = answer.data;
        
        refreshDataboard();        
    }else{
        let container = document.getElementById("container");
        let empty = document.getElementById("empty_list");
        empty.style.display = "block";
        container.style.display = "none";
    }
}

// Retourne la liste des id des étudiants
function returnListe_id(){
    return liste_etudiant_id;
}

// FOnction qui récupère les étudiants et les places dans les tableaux
function refreshDataboard(){
    for (let i = 0; i < result.length; i++) {
        if(liste_etudiant_id.indexOf(result[i].id_utilisateur) < 0){
            liste_etudiant_id.push(result[i].id_utilisateur);
            liste_etudiant_nom.push(result[i].nom_utilisateur);
            liste_etudiant_prenom.push(result[i].prenom_utilisateur);
            liste_etudiant_filiere.push(result[i].type_utilisateur);
        }
    }
    
    
    
    

    // Ajout de tous les étudiants dans la liste
    for (let i = 0; i < liste_etudiant_id.length; i++) {
        insertRow(liste_etudiant_id[i],
            liste_etudiant_nom[i],
            liste_etudiant_prenom[i],
            liste_etudiant_filiere[i]);
    }
    addLink();
}

// Fonction qui ajoute une ligne dans la liste des étudiants
function insertRow(id, nom, prenom, filiere){
    var tbodyRef = document.getElementById('donnee').getElementsByTagName('tbody')[0];

    // Création d'une nouvelle ligne
    var newRow = tbodyRef.insertRow();
    newRow.setAttribute("data-id",id);

    // Création de la cellule Nom
    var newCell = newRow.insertCell();
    // Ajout du texte dans la cellule Nom
    var newText = document.createTextNode(nom);
    newCell.appendChild(newText);

    // Création de la cellule Prenom
    newCell = newRow.insertCell();
    // Ajout du texte dans la cellule Prenom
    newText = document.createTextNode(prenom);
    newCell.appendChild(newText);

    // Création de la cellule Filière
    newCell = newRow.insertCell();
    // Ajout du texte dans la cellule Filière
    newText = document.createTextNode(filiere);
    newCell.appendChild(newText);

    // Création de la cellule CVtheque
    newCell = newRow.insertCell();
    // Ajout du bouton dans la cellule CVtheque
    var btn = document.createElement('input');
    // On paramètre le bouton
    btn.setAttribute('id',id);
    btn.setAttribute('type','image');
    btn.setAttribute('class','btn');   
    btn.setAttribute('src','views/img/dashboard/see.png');
    // Ajout du bouton dans la cellule CVtheque
    newCell.appendChild(btn);    
}

// Fonction qui redirige vers la cvtheque de l'étudiant en question
function redirect(id){
    window.location=`?page=cvtheque&id=${id.currentTarget.data}`;
}

// Pour chaque bouton de la classe "btn" on lui ajoute un lien vers la CVthèque de l'étudiant
function addLink(){
    // On récupère tous les boutons
    var btns = document.getElementsByClassName("btn");

    // Pour chaque bouton on associe le lien vers la CVtheque associée
    Array.prototype.forEach.call(btns, function(btn) {
        let id = btn.getAttribute("id");
        
        btn.addEventListener("click",redirect);
        btn.data = id;
    });
}
