$(function () {

    const keys = {"nom": String, "prenom": String, "mail": String, "type_utilisateur": "select",'etudiant':Boolean, "valide": Boolean};

    const input_filtersInit={"nom":"",'prenom':"",'mail':"",'etudiant':"",'type_utilisateur':'','valide': ''};
    var input_filters=clone(input_filtersInit)

    var table_filter = "id_utilisateur"; //use for dynamicSort
    let sortOrder = 1
    let users = [];

    let url = "/api/user/";

    updateTable();

    function updateTable() {

        ajax(url, "GET", undefined, result => {
                users = result;

                users = users.sort(dynamicSort(table_filter));
                fillUsers(users)
            }
        )
    }

    function dynamicSort(property) {

        return function (a, b) {
            /* next line works with strings and numbers,
             * and you may want to customize it to your needs
             */
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

    function updateTablebyNestedFilter(){
        let filtered_users=nestedFilter();
        fillUsers(filtered_users);
        updateNgLignes();
    }
    function th_filters() {
        $("#user_table th[data-label] span").on("click", function (event) {
            let th=$(this).closest('th');
            let filter=$(th).data('label');
            let tmp = table_filter;
            table_filter = filter;
            if (tmp === table_filter) sortOrder *= -1;

            let filteredusers=nestedFilter();
            filteredusers = filteredusers.sort(dynamicSort(table_filter));
            fillUsers(filteredusers)
        })

        $("#user_table th #filter_button").on("click",function (){
            updateTablebyNestedFilter();
        });

        $("#user_table th #refresh_button").on("click",function (){
            $('.input_filters').val('')
            $('.input_filters[type=checkbox]').prop('checked',false);
            input_filters=clone(input_filtersInit);
            fillUsers(users);
        });

        $("#user_table th .input_filters").on('input',function (e){

            let filter_value=$(this).data("filter")
            let isCheckbox=$(this).is(":checkbox");
            let val=$(this).val();
            if(isCheckbox){
                val=$(this).is(':checked')?"1":'0';
            }
            input_filters[filter_value]=val;
            updateTablebyNestedFilter()

        })

        $("#select_all").on('input',function (e){
            var selected = e.target.checked;

            $('.select_user_box').prop("checked",selected);
            updateNgLignes()
        })
    }

    function updateNgLignes(){
        let ids=getSelectedIds();
        $("#nbLigne").text(ids.length);
    }

    function getSelectedIds(){
        let ids=[]
        $('.select_user_box:checked').map(function (){
            let parent=$(this).closest('tr');
            let id=parent.data("id");
            ids.push(id)
        })
        return ids;
    }

    function clone(object){
        return Object.assign({},object);
    }


    function nestedFilter(){
        var filterKeys = Object.keys(input_filters);
        return users.filter(function (eachObj){
            return filterKeys.every(function (eachKey){
                if(input_filters[eachKey]===''){
                    return true;
                }
                let input_val=input_filters[eachKey]
                let obj_val=eachObj[eachKey];
                if(keys[eachKey]===String){
                    return obj_val.toLowerCase().includes(input_val.toLowerCase())
                }else{
                    return input_val===obj_val;
                }
            })
        })
    }


    function clearUserTable(tbody) {
        $(tbody).empty();
    }

    function setTableHeader(table, keys) {
        let thead_exist = $(".thead");
        if (thead_exist.length > 0) return
        let thead = document.createElement("thead");
        thead.className = "thead"
        let trh = document.createElement("tr");

        let th0 = document.createElement("th");
        let div_content0=document.createElement('div')
        div_content0.className="th_div";
        let select_all=document.createElement("input");
        select_all.type="checkbox";
        select_all.id="select_all"

        let text_span = document.createElement("span");
        let text = document.createTextNode("#");
        text_span.appendChild(text)

        div_content0.appendChild(text_span);
        div_content0.appendChild(select_all)
        th0.appendChild(div_content0)
        trh.appendChild(th0)

        Object.keys(keys).map(function (key) {
            let th = document.createElement("th");
            let div_content=document.createElement('div')
            div_content.className="th_div";

            let text_span = document.createElement("span");
            let text = document.createTextNode(key);
            text_span.appendChild(text)
            div_content.appendChild(text_span)
            createInput(div_content,key)

            th.appendChild(div_content);
            th.setAttribute("scope", 'col')
            th.setAttribute("data-label",key);
            trh.appendChild(th);
        });

        let th = document.createElement("th");
        let filterButton=document.createElement("input");
        filterButton.type="image";
        filterButton.id="filter_button";
        filterButton.className="btn";
        filterButton.src="views/img/filter_icon.png"

        th.appendChild(filterButton);
        trh.appendChild(th);

        let refresh = document.createElement("th");
        let refreshButton=document.createElement("input");
        refreshButton.type="image";
        refreshButton.id="refresh_button";
        refreshButton.className="btn";
        refreshButton.src="views/img/refresh_icon.png"

        refresh.appendChild(refreshButton);
        trh.appendChild(refresh);

        thead.appendChild(trh);
        table.append(thead);

        th_filters();
    }

    function createInput(th,key){
        let input_filter=document.createElement("input")

        switch (keys[key]){
            case String:{
                input_filter.type="text";
                break
            }
            case Boolean:{
                input_filter.type="checkbox";
                break
            }
            case "select":{
                return createTypeInput(th,key);
            }

        }

        input_filter.className="input_filters"
        input_filter.setAttribute('data-filter',key)
        th.appendChild(input_filter);
    }

    function createTypeInput(th,key){
        return ajax(url+'/types','GET',undefined,function (result){
            let input=document.createElement("select");
            input.className="type_select"
            input.setAttribute('data-filter',key)

            $(input).on('change',function (e){
                let filter_value=$(this).data('filter');
                input_filters[filter_value]=$(this).val();

                updateTablebyNestedFilter();
            })

            let el=document.createElement('option');
            el.textContent = "--Choisir type";
            el.value = "";
            input.appendChild(el);

            for (let type in result){
                let el=document.createElement('option');
                el.textContent = result[type].libelle;
                el.value = result[type].libelle;

                input.appendChild(el);
            }
            th.appendChild(input)
        })
    }

    function strToBool(s) {
        // will match one and only one of the string 'true','1', or 'on' rerardless
        // of capitalization and regardless off surrounding white-space.
        //
        regex = /^\s*(true|1|on)\s*$/i

        return regex.test(s);
    }

    function fillBody(table, users, keys) {
        let tbody = $(".tbody");

        if (tbody.length > 0) {
            clearUserTable(tbody)
        } else {
            tbody = $(document.createElement("tbody"));
            tbody.addClass("tbody");
        }

        let checked=$("#select_all").is(":checked");

        users.map(user => {
            let tr = document.createElement("tr");
            tr.setAttribute("data-id", user.id_utilisateur)

            let td=document.createElement("td");
            td.className="center";
            let select_user_box=document.createElement("input")
            select_user_box.className="select_user_box";
            select_user_box.type="checkbox";
            select_user_box.checked=checked

            td.appendChild(select_user_box)
            tr.appendChild(td);

            Object.keys(keys).map(function (field) {
                if (user.hasOwnProperty(field)) {
                    let td = document.createElement('td');
                    let value = user[field]
                    let type = keys[field];
                    if (type === Boolean) {
                        if (strToBool(value) === false) {
                            value = "Non";
                        } else {
                            value = "Oui";
                        }
                    }
                    let text = document.createTextNode(value)
                    td.appendChild(text);
                    tr.appendChild(td);
                }
            });

            let modifyBtn = document.createElement('td');
            let btn_text = document.createTextNode("✏️");
            $(modifyBtn).addClass("modify btn");
            modifyBtn.appendChild(btn_text)
            tr.appendChild(modifyBtn);

            let deleteBtn = document.createElement('td');
            let dlt_text = document.createTextNode("🗑️");
            $(deleteBtn).addClass("delete btn");
            deleteBtn.appendChild(dlt_text)

            tr.appendChild(deleteBtn);
            tbody.append(tr);
        })
        table.append(tbody)

        modify(users);
        $('.select_user_box').on('input',function (e){
            updateNgLignes();
        })
    }

    function fillUsers(users) {
        let table = $(document.getElementById('user_table'));
        setTableHeader(table, keys);
        fillBody(table, users, keys);
        deleteUser();
    }

    function filter(users, key, value) {

        return users.filter(user => {
            return convertToType(value, user[key]) === value
        })
    }

    function convertToType(t, e) {
        return (t.constructor)(e);
    }

//############################# MODAL PART #############################//
    const modal = $("#modal");

// Get the button that opens the modal
    const btn = $(".modal_button");

// Get the <span> element that closes the modal
    const closebtn = $(".close");

    function showModal(bool, id = undefined, users = undefined) {
        if (bool === true) {
            modal.removeClass("hide");
            if (id && users) {
                let user = users.filter(user => {
                    return convertToType(id, user.id_utilisateur) === id
                })[0];
                updateModalForm(user);
            }
        } else {
            modal.addClass('hide');
        }
    }

// When the user clicks on the button, open the modal
    btn.on('click', function (event) {
        showModal(true)
    });

// When the user clicks on <span> (x), close the modal
    closebtn.on('click', function () {
        showModal(false)
    })

// When the user clicks anywhere outside of the modal, close it
    $(window).on("click", function (event) {

        if (event.target.id === modal.attr("id")) {
            showModal(false);
        }
    })

    function modify(users) {
        $(".modify").on("click", function (event) {
            let btn = $(this);
            let tr = btn.closest("tr");
            let id = tr.data("id");
            showModal(true, id, users)
        })
    }

    function fillFormValues(user) {
        let form_values = ["nom", "prenom", "mail", "id_type_utilisateur", "valide"];
        form_values.map(key => {
            let input = $(`#update_form [name=${key}]:not([type=hidden])`)

            let type = input.prop('type')

            if (type === 'checkbox') {
                let isChecked = strToBool(user[key]);
                input.prop('checked', isChecked);
                input.val(isChecked ? 1 : 0);

            } else {
                input.val(user[key]);
            }
        })
    }

    function updateModalForm(user) {

        fillFormValues(user);
        let update_form = $("#update_form");

        update_form.off("submit");
        update_form.on("submit", function (e) {
            e.preventDefault();

            let id = user.id_utilisateur

            ajax(url + id, 'PUT', $("#update_form").serialize(), function (result) {
                if (result.status === true) {
                    updateTable();
                    showModal(false);
                }
            })

        })
    }

    $("input[type=checkbox]").on("change", function () {
        $(this).val($(this).prop('checked') ? 1 : 0);
    })


    function deleteUser() {
        $(".delete").on("click", function (event) {
            let btn = $(this);
            let tr = btn.closest("tr");
            let id = tr.data("id");

            let user = filter(users, "id_utilisateur", id)[0];
            var r = confirm("Voulez vous vraiment supprimer l'utilisateur: " + user.prenom + " " + user.nom)
            if (r === true) {
                ajax(url + "/" + id, "DELETE", undefined, function (result) {
                    if (result.status === true) {
                        updateTable()
                    }
                })
            }

        })
    }


    $("#user_add_form").on("submit", function (e) {
        e.preventDefault();
        $('.custom_msg').addClass('hide')
        ajax(url, "POST", $(this).serialize(), function (result) {
                if (result.status === true) {
                    updateTable();

                    $("#user_add_form")[0].reset();
                } else {
                    let msg = $(".custom_msg");
                    msg.text(result.message);
                    msg.removeClass('green');
                    msg.removeClass('hide');
                }
            }
        )
    })

    var pass = document.getElementById('mot_de_passe');
    var pass_confirm = document.getElementById('password_confirmation');
    var custom_msg = $('.custom_msg');

    function isValidPassword(password) {
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\\sa-zA-Z\\d])(?=.{8,})");
        return strongRegex.test(password);
    }

    function checkStrength(event) {
        event.preventDefault()
        let val = event.target.value;
        let confirm_password = pass_confirm.value;
        if (!isValidPassword(val)) {
            if(val===''){
                $(pass).css('borderColor', '#C4C4C4');
                custom_msg.addClass('hide');
                if(confirm_password===''){
                    $(pass_confirm).css('borderColor', '#C4C4C4');
                }

            }else{
                $(pass).css('borderColor', 'red');
                custom_msg.text("Mot de passe faible : >=8 (minuscule, majuscule, chiffre, caractère spécial)")
                custom_msg.removeClass('hide');
            }
            if (pass_confirm.value !== '') $(pass_confirm).css('borderColor', 'red');
        } else {
            if (pass_confirm.value !== '' && pass_confirm.value !== val)  {
                $(pass_confirm).css('borderColor', 'red');
            }

            $(pass).css('borderColor', 'green');

            if (confirm_password === val) {
                $(pass_confirm).css('borderColor', 'green');
                custom_msg.text("")
                custom_msg.addClass("hide");
            }else{
                custom_msg.text("Mots de passe non identiques")
                custom_msg.removeClass("hide");
                $(pass_confirm).css('borderColor', 'red');
            }
        }
    }

    function pass_input_change(event) {
        event.preventDefault()

        let confirm = event.target.value;
        let first_pass = pass.value;

        let color = "red";

        if (first_pass === confirm) {

            if(isValidPassword(first_pass)){
                custom_msg.text("Mots de passe identiques")
                custom_msg.addClass('hide'); //on affiche pas le message
                color = "green";
            }else{
                custom_msg.text("Les mots de passes ne sont pas valides")
                if(first_pass===''){
                    color="#C4C4C4";
                }
            }

        } else {
            custom_msg.text("Mots de passe non identiques")
            custom_msg.removeClass('hide')
        }

        if (color === "green") {
            custom_msg.addClass("green");
        } else {
            custom_msg.removeClass("green")
        }

        pass_confirm.style.borderColor = color;
    }

    pass.addEventListener('input', (event) => checkStrength(event));
    pass_confirm.addEventListener('input', (event) => pass_input_change(event));

    let action_id_select=$("#action_bar select[name=action_id]");
    let action_form=$("#action_bar");
    let type_modif_select=$("#action_bar select[name=id_type_utilisateur]");
    let action_bar_checkbox=$("#action_bar_checkbox");

    function hideAll(){
        type_modif_select.addClass('hide')
        action_bar_checkbox.addClass('hide')
    }

    action_form.on('submit',function (e){
        e.preventDefault();

        let v=confirm("Souhaitez vous vraiment réaliser cette action ?")
        if(v===true){
            let ids=getSelectedIds()
            let actions=action_form.serializeArray()
            actions.push({
                name : 'ids',
                value: ids
            })

            $.ajax({
                url: url,
                method: 'PUT',
                data: actions,
            }).done(function(res){
                updateTable();
                updateNgLignes();
            })
        }
    })

    action_bar_checkbox.on('change',function (e){
        let checked=e.target.value;
        $(this).val(checked);
    })

    action_id_select.on('change',function (e){
        let value=e.target.value;

        hideAll()
        switch (value) {
            case '1':{
                action_bar_checkbox.removeClass('hide');
                break
            }
            case '2':{
                type_modif_select.removeClass('hide');
                break
            }
        }
    })



})
