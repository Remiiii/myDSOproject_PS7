var pass = document.getElementById('mot_de_passe');
var pass_confirm = document.getElementById('password_confirmation');
var custom_msg = $('.errorMsg');

function isValidPassword(password) {
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\\sa-zA-Z\\d])(?=.{8,})");
    return strongRegex.test(password);
}

function checkStrength(event) {
    event.preventDefault()
    let val = event.target.value;
    let confirm_password = pass_confirm.value;
    if (!isValidPassword(val)) {
        if(val===''){
            $(pass).css('borderColor', '#C4C4C4');
            custom_msg.addClass('hide');
            if(confirm_password===''){
                $(pass_confirm).css('borderColor', '#C4C4C4');
            }

        }else{
            $(pass).css('borderColor', 'red');
            custom_msg.text("Mot de passe faible : >=8 (minuscule, majuscule, chiffre, caractère spécial)")
            custom_msg.removeClass('hide');
        }
        if (pass_confirm.value !== '') $(pass_confirm).css('borderColor', 'red');
    } else {
        if (pass_confirm.value !== '' && pass_confirm.value !== val)  {
            $(pass_confirm).css('borderColor', 'red');
        }

        $(pass).css('borderColor', 'green');

        if (confirm_password === val) {
            $(pass_confirm).css('borderColor', 'green');
            custom_msg.text("")
            custom_msg.addClass("hide");
        }else{
            custom_msg.text("Mots de passe non identiques")
            custom_msg.removeClass("hide");
            $(pass_confirm).css('borderColor', 'red');
        }
    }
}

function pass_input_change(event) {
    event.preventDefault()

    let confirm = event.target.value;
    let first_pass = pass.value;

    let color = "red";

    if (first_pass === confirm) {

        if(isValidPassword(first_pass)){
            custom_msg.text("Mots de passe identiques")
            custom_msg.addClass('hide'); //on affiche pas le message
            color = "green";
        }else{
            custom_msg.text("Les mots de passes ne sont pas valides")
            if(first_pass===''){
                color="#C4C4C4";
            }
        }

    } else {
        custom_msg.text("Mots de passe non identiques")
        custom_msg.removeClass('hide')
    }

    if (color === "green") {
        custom_msg.addClass("green");
    } else {
        custom_msg.removeClass("green")
    }

    pass_confirm.style.borderColor = color;
}

pass.addEventListener('input', (event)=>checkStrength(event));
pass_confirm.addEventListener('input', (event)=>pass_input_change(event));

$(function (){
    $(".errorMsg").text("")
    $("#register_form").submit(function (e){
        //prevent Default functionality
        e.preventDefault();

        //get the action-url of the form
        var actionurl = e.currentTarget.action;

        //do your own request an handle the results
        $.ajax({
            url: actionurl,
            type: 'POST',
            contentType: 'application/json;',
            data: $("#register_form").serialize(),
            success: function(data) {
                if(data.status===true){
                    window.location='?page=login_form';
                }else{
                    $(".errorMsg").text(data.message).removeClass('hide');
                }
            },
        })
    })
})