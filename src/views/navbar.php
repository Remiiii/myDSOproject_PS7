<nav>
    <div class="navbar-footer-content navbar-content">
        <div class="navbar-left">
            <a href="<?= HTTP_URL . "home" ?>" class="navbar-link">Accueil</a>
            <a href="#" class="navbar-link">Contact</a>
        </div>

        <p class="navbar-title">ENSIBS stages</p>

        <div class="navbar-right">
            <?php if (isset($_SESSION["user"])) {
                $user=$_SESSION["user"];
                $nom=$user->nom;
                ?>
                <a href="<?= HTTP_URL . "dashboard" ?>" class="navbar-link"><?= "Tableau de bord de ".$nom ?></a>
                <button id="logout_button" class="navbar-link">Se deconnecter</button>
            <?php } else { ?>
                <a href="<?= HTTP_URL . "login_form" ?>" class="navbar-link">Se connecter</a>
                <a href="<?= HTTP_URL . "create_account_form" ?>" class="navbar-link">S'inscrire</a>
            <?php } ?>
        </div>
    </div>
</nav>
