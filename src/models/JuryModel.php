<?php
namespace App\Models;

require_once "models/Model.php";
require_once 'models/TypeDocumentModel.php';

class JuryModel extends Model{
  protected string $table = "jury";
  protected bool $auto_increment = true;
  protected string $primary_key = "id_jury";

  protected array $attributes = [
      'id_jury'=>null,
      'id_planning' => "required",
      'id_salle' => "required",
      'id_utilisateur1' => "required",
      'id_utilisateur2' => "required",
      'date_deb' => "required",
      'date_fin' => "required"
  ];

  private function save_jury(array $request){

    //On verifie que tout les champs soient remplis
    /*
    $ret=$this->checkFieldsExist($request,$required_fields);

    if($ret["status"]===false){
        $ret["message"]="Tous les champs doivent être remplis";
        return $ret;
    }
    */
    //// TODO: les verifications
    //On lance la requete
    return $this->insert($request);
  }
}
 ?>
