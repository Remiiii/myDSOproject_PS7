<?php

namespace App\Models;

require_once 'Model.php';
require_once "models/vues/PlanningVue.php";

use App\Models\Model;
use App\Models\Vues\PlanningVue;

class PlanningModel extends Model
{
    protected string $table = 'planning';
    protected string $primary_key = "id_planning";
    protected bool $auto_increment = true;

    protected array $attributes = [
        'id_planning' => null,
        'type_planning' => "required",
        'valide' => "required",
        'date_deb' => "required",
        'date_fin' => "required",
        'duree_creneau' => "required",
        'nom' => "required"
    ];
}