<?php

namespace App\Models;


require_once 'Model.php';

use App\Models\Model;

class TypeUtilisateurModel extends Model
{
    protected string $table = "type_utilisateur";
    protected bool $auto_increment = true;

    protected string $primary_key='id_type_utilisateur';
    protected array $attributes = [
        'id_type_utilisateur' => null,
        'libelle' => 'required',
        'etudiant' => 'required',
        'besoin_validation' => 'required'
    ];

}