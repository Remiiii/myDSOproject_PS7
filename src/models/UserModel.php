<?php

namespace App\Models;

require_once 'Model.php';
require_once "models/vues/UtilisateurVue.php";
require_once "models/TypeUtilisateurModel.php";

use App\Models\Model;
use App\Models\Vues\UtilisateurVue;

class UserModel extends Model
{
    protected string $table = 'utilisateur';
    private static string $passwordSalt = 'a1é&çéà_!%{PASSWORD}%$^*</+.^à';

    protected string $primary_key="id_utilisateur";
    protected bool $auto_increment=true;

    protected array $attributes = [
        'id_utilisateur'=>null,
        'prenom'=>"required",
        'nom'=>"required",
        'mail'=>"required",
        'mot_de_passe'=>"required",
        'id_type_utilisateur'=>"required",
        "valide" => null
    ];

    protected array $hidden = [
        'mot_de_passe'
    ];

    private function hashPassword($password)
    {
        $salted_pass = str_replace('%{PASSWORD}%', $password,
            self::$passwordSalt); //remplace %{PASSWORD}% par le mot de passe
        return hash('sha512', $salted_pass); //sha512 is 128 characters long
    }


    private function verifyPassword($hash, $password)
    {
        return $hash === $this->hashPassword($password);
    }

    private function findByEmail(string $mail)
    {
        $db = $this::getInstance();
        $query = "SELECT * FROM $this->table WHERE mail LIKE :mail;";
        $stmt = $db->prepare($query);
        $stmt->bindParam(':mail', $mail);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }


    private function checkPasswordStrength($password)
    {
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\sa-zA-Z\d]@', $password);

        if (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
            return [
                'status' => false,
                'message' => 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.'
            ];
        } else {
            return
                [
                    'status' => true,
                    'message' => 'password valid'
                ];
        }
    }

    private function checkEmail($mail)
    {
        if (!empty($mail)) {
            $mail = filter_var($mail, FILTER_SANITIZE_EMAIL); //remove non authorized characters

            if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                return $mail;
            }
        }
        return false;
    }

    public function login($request)
    {
        $mail_field="mail";
        $password_field="mot_de_passe";

        $ret=$this->checkFieldsExist($request,[$mail_field,$password_field]);

        $msg="Email ou mot de passe incorrect";
        if($ret["status"]===true){
            $mail=$request[$mail_field];
            $user = $this->findByEmail($mail);
            $password=$request[$password_field];
            if (!empty($user)) {

                $vue_user=(new UtilisateurVue())->findByParam("id_utilisateur",$user->id_utilisateur);
                $user_v=$vue_user["data"][0];

                if((bool)$user->valide===true || strtolower($user_v->type_utilisateur)==="exterieur"){
                    $isLogged=$this->verifyPassword($user->mot_de_passe, $password);
                    $_SESSION["user"]=$user_v;
                    $ret["message"]="Success";
                    if($isLogged)return ["status"=>true,"message"=>"success"];
                }else{
                    $msg= "Votre compte est en attente de validation";
                }
            }
        }
        return ["status"=>false,"message"=>$msg];
    }

    public function insertAdmin(array $request): array
    {
        $required_fields=["nom","prenom","mail","mot_de_passe","password_confirmation","id_type_utilisateur","valide"];

       return $this->save_user($request,$required_fields);
    }

    private function save_user(array $request,array $required_fields){
        $request=array_intersect_key($request,array_flip($required_fields));

        $ret=$this->checkFieldsExist($request,$required_fields);

        if($ret["status"]===false){
            $ret["message"]="Tous les champs doivent être remplis";
            return $ret;
        }



        $is_admin=$this->checkType("Administrateur");

        if($is_admin===false){
            $register_page_ids=[4,5,6,7];
            $id_type_utilisateur=$request["id_type_utilisateur"];

            if(in_array($id_type_utilisateur,$register_page_ids)===false){
                return ["status"=>false,"message"=>"Type utilisateur non existant"];
            }
        }


        $request['nom']=ucfirst($request['nom']);
        $request['prenom']=ucfirst($request['prenom']);

        $password=$request["mot_de_passe"];
        $password_confirmation=$request["password_confirmation"];
        $ret["status"]=false; //reinitialise pour autre verification

        $mail=$request["mail"]; //required donc pas null
        if(strlen($mail)>255){
            $ret['message'] = "Mail trop long, 255 caractères maximum";
            return $ret;
        }

        $mail = $this->checkEmail($mail);

        if ($this->alreadyExists('mail', $mail)["status"]===true) {
            $ret['message'] = "Mail déja utilisé";
            return $ret;
        }

        if ($password !== $password_confirmation) {
            $ret['message'] = 'Les mots de passe ne sont pas identiques';
            return $ret;
        };

        $pass_strength = $this->checkPasswordStrength($password);
        if ($pass_strength['status'] === true) {
            $pass_hash = $this->hashPassword($password);

            $request["mot_de_passe"]=$pass_hash; //rename password en mot_de_passe
            $request["mail"]=$mail;
            return $this->insert(
                $request
            );
        } else {
            return $pass_strength;
        }
    }

    public function register(
        array $request
    ) {

        $required_fields=["nom","prenom","mail","mot_de_passe","password_confirmation","id_type_utilisateur"];

        return $this->save_user($request,$required_fields);

    }

    public function checkType(string $type){
        if(isset($_SESSION)){
            if (isset($_SESSION['user'])){
                $user=$_SESSION['user'];
                if(strtolower($user->type_utilisateur)===strtolower($type))return true;
            }
        }
        return false;
    }

}