<?php

namespace App\Models;

use App\Models\Vues\Vue;

require_once "models/vues/Vue.php";

abstract class Model extends Vue
{
    protected string $primary_key='id';
    protected bool $auto_increment=false;
    protected array $attributes = array();
    protected array $hidden = array();

    protected function is_bool($value){
        if($value==='1' || $value==='0' ||$value===1 ||$value===0 ||$value===true ||$value===false){
            return true;
        }
        return false;
    }

    protected function isEmpty($value){

        if(empty($value) && !$this->is_bool($value)){
            return true;
        }

        return false;
    }

    protected function checkFields($fields){

        $ret=["status"=>true,"message"=>[]];
        if(!$this->auto_increment){

            if($this->isEmpty($fields[$this->primary_key])){
                $ret["status"]= false;
                array_push($ret["message"],"The key : '".htmlspecialchars($this->primary_key)."'is required");
            }
        }
        foreach ($this->attributes as $key=>$value){
            switch ($value){
                case 'required':
                    if($this->isEmpty($fields[$key])){
                        $ret["status"]=false;
                        array_push($ret["message"],"The Key: '".htmlspecialchars($key)."' is required");
                        break;
                    }
            }
        }
        return $ret;
    }

    protected function checkFieldsExist(array $request,array $fields){

        $ret=["status"=>true,"message"=>[]];

        foreach ($fields as $key){
            if($this->isEmpty($request[$key])){
                array_push($ret["message"],"The Key: '".htmlspecialchars($key)."' is required");
                break;
            }
        }
        if(sizeof($ret["message"])>0)$ret["status"]=false;
        return $ret;
    }

    private function getVisibleFields(): string
    {
        $diff = array_diff(array_keys($this->attributes), $this->hidden); //remove hidden attributes from request
        $fields = implode(',', $diff);
        return $fields;
    }

    /**
     * Retourne un tableau contenant toutes les champs qui sont dans fields mais qui sont aussi dans les attributs du model
     * @param $fields array Les champs de la requête
     * @return array la tableau de retour avec uniquement les champs existant dans les attributs du model
     */

    private function filterFieldsByAttribute(array $fields):array{
        $keys=array_keys($this->attributes);
        return array_filter($fields, function($v) use ($keys) {
            return in_array($v, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function insert(array $row,bool $getId=false):array{
        $db=self::getInstance();

        $fields=$this->filterFieldsByAttribute($row); //trie les champs

        $checkFields=$this->checkFields($fields);
        if($checkFields["status"]===false){
            return $checkFields;
        }

        $implode_field=implode(', ',array_keys($fields));
        $implode_values=implode(', ',array_fill(0,sizeof($fields),"?"));

        $query="INSERT INTO `$this->table` ($implode_field) VALUES ( $implode_values );";
        $stmt=$db->prepare($query);
        try{
            $stmt->execute(array_values($fields));
            $checkFields["message"]="Insertion reussie";
            if($getId===true)$checkFields["id"]=$db->lastInsertId();
        }catch (\PDOException $e){
            return ["status"=>false,"message"=>"Impossible d'insérer, types imcompatibles"];
        }
        return $checkFields;
    }

    /**
     * retourne un tableau avec les champs défini dans attributes en enlevant ceux hidden
     * @return array
     * @throws \Exception
     */
    public function getAll(): array
    {
        $db = self::getInstance();
        $fields = $this->getVisibleFields();
        $query = "SELECT $fields FROM $this->table";
        $stmt = $db->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_CLASS);
    }

    /**
     * Effectue une recherche par clé primaire
     *
     * exemple de value return:
     * ["status"=> true, "data" => ["id_document" => 1, ...]]
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function find($id):array
    {
        $result=$this->findByParam($this->primary_key,$id);
        if($result["status"]===true){
            $result["data"]=$result["data"][0]; //recupere l'unique element car clé primaire
        }
        return $result;
    }

    /**
     * recherche par un seul paramètre
     * ["status"=> true, "data" => [["id_document" => 1,...],["id_document" => 2,...]]]
     * @param string $param
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function findByParam(string $param,$value):array{
        $db = self::getInstance();
        $fields = $this->getVisibleFields();

        $query = 'SELECT '.$fields.' FROM ' . $this->table. ' WHERE '.$param.' = :p;';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':p',$value);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_CLASS);

        $ret=["status"=>!empty($result)];
        $ret["data"]=$result;
        return $ret;
    }

    function findByMultipleParams(array $params,array $values):array{
        $ret=["status"=>false,"message"=>null];
        $paramsSize=sizeof($params);
        $valuesSize=sizeof($values);
        if($paramsSize!==$valuesSize){
            $ret["message"]="number of values and params are not the same";
            return $ret;
        }
        if($paramsSize<1){
            $ret["message"]="at least one param must be given";
            return $ret;
        }
        $db = self::getInstance();
        $fields = $this->getVisibleFields();

        $wherePart="WHERE $params[0] = :$params[0]";
        for($i=1;$i<$paramsSize;$i++){
            $wherePart.=" AND $params[$i] = :$params[$i]";
        }
        $query = 'SELECT '.$fields.' FROM ' . $this->table.' '.$wherePart.";";


        $stmt = $db->prepare($query);
        for($j=0;$j<$paramsSize;$j++){
            $stmt->bindParam(":$params[$j]",$values[$j]);
        }
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_CLASS);

        if(!empty($result)){
            $ret=["status"=>true,"data"=>$result];
        }else{
            $ret["message"]="Pas de données";
        }
        return $ret;
    }

    public function delete($id,string $key=null): array
    {
        if(empty($key)){
            $key=$this->primary_key;
        }
        $db = self::getInstance();
        $query = 'DELETE FROM ' . $this->table. ' WHERE '.$key.' = :id;';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id',$id);
        $stmt->execute();

        $cpt=$stmt->rowCount();
        return ["status"=>($cpt>0),"deletedRows"=>$cpt];
    }

    /**
     * @throws \JsonException
     */
    public function updateOne($id,array $values){
        $db = self::getInstance();

        $vals=array_intersect_key($values, $this->attributes); //on enleve les valeurs qui ne sont pas des attributs de la table
        if(isset($vals[$this->primary_key])){
            unset($vals[$this->primary_key]); //on ne peut pas update la clé primaire
        }

        if(empty($vals)){
            throw new \JsonException("No value given",400);
        }
        $setparts="";

        foreach ($vals as $key=>$name){
            $setparts.= " $key = :$key,";
        }
        $setparts=rtrim($setparts ,",");
        $pk=$this->primary_key;
        $query = 'UPDATE '.$this->table.' SET '.$setparts." WHERE $pk = :$pk ;";
        $stmt = $db->prepare($query);

        foreach ($vals as $key=>$name){
            $stmt->bindValue(":$key",$name);
        }
        $stmt->bindValue(":$pk",$id);


        $status=$stmt->execute();
        return ["status"=>$status,"updatedRows"=>$stmt->rowCount()];
    }

    public function updateByParam($params){

    }


}