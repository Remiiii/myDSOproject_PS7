<?php

namespace App\Models;

require_once "models/Model.php";
require_once 'models/TypeDocumentModel.php';

define('KB', 1024);
define('MB', 1048576);

class DocumentModel extends Model
{
    protected string $table = "document";
    protected bool $auto_increment = true;
    protected string $primary_key = "id_document";

    private int $MAX_SIZE = 5; //max size en Mega Bytes
    private static ?string $documentDir="Documents"; //dossier ou chemin a partir du repertoire courant
    private static ?string $documentRoot=null; //chemin absolu
    private array $authorized_types = ["application/pdf"=>"pdf"];

    protected array $attributes = [
        "id_document" => null, //c'est la clé primaire donc obligatoirement required
        "id_utilisateur" => "required",
        "id_type_document" => "required",
    ];

    private function checkSize($size)
    {
        $valid = $size < $this->MAX_SIZE*MB;
        return ["status" => $valid, "message" => $valid === false ? "Fichier trop volumineux, taille max: $this->MAX_SIZE MB" : null];
    }

    private function checkMimeType($mime)
    {
        return array_key_exists($mime,$this->authorized_types);
    }

    private function hasAlreadyDocument($type)
    {
        $user_id = $_SESSION["user"]->id_utilisateur;
        $result = $this->findByMultipleParams(["id_utilisateur", "id_type_document"], [$user_id, $type]);
        return $result["status"];
    }

    public function check_file_uploaded_name ($filename,$mimetype)
    {
        $isValid=false;
        if($this->checkMimeType($mimetype)){
            $extension=$this->authorized_types[$mimetype];
            $regex=  "`^[^\.]{1,65}\.$extension$`";
            $isValid=(bool)preg_match($regex, $filename);
        }
        return $isValid;
    }

    /**
     * Retourne le chemin ou sont localisés les documents
     * @param $id int nom du fichier
     * @param $mimetype string  type extension
     * @return string le nom complet avec extension
     */
    private function generateFilePath(int $id,string $mimetype): string{
        $prefix=$_SERVER['DOCUMENT_ROOT'];
        if(is_dir(self::$documentRoot)){
            $prefix=self::$documentRoot;
        }else{
            $prefix.="/".self::$documentDir;
        }

        return $prefix."/".$id.".".$this->authorized_types[$mimetype];
    }

    public function moveTmpFileToDocuments($tmp_name,$id,$mimetype):bool{
        return move_uploaded_file($tmp_name,$this->generateFilePath($id,$mimetype));
    }


    public function isValidDocument($document, $type,$check_doublons=false)
    {
        $ret = ["status" => false, "message" => null];

        if($check_doublons===true){
            $hasAlready = $this->hasAlreadyDocument($type);
            if ($hasAlready) {
                http_response_code(403);
                $ret["message"] = "Ce document existe deja";
                return $ret;
            }
        }

        $mimetype = $document["type"];

        if(!$this->checkMimeType($mimetype)){
            http_response_code(403);
            $ret["message"] = "Ce type de document n'est pas authorisé";
            return $ret;
        };

        $document_size = $document["size"];
        $size_result=$this->checkSize($document_size);
        if($size_result["status"]===false){
            http_response_code(413);
            $ret["message"] = $size_result["message"];
            return $ret;
        }

        $ret["status"]=true;
        return $ret;
    }

    public function downloadFile(\stdClass $document,\stdClass $user=null){
        $mimetype='application/pdf';

        $id=$document->id_document;

        $type_document_model=new TypeDocumentModel();

        $type_doc=$type_document_model->find($document->id_type_document);

        if($type_doc["status"]===false){
            throw new \JsonException("Fichier introuvable: ",400);
        }
        $libelle=$type_doc["data"]->libelle;
        $filename=$libelle.'.'.$this->authorized_types[$mimetype];

        if($user!==null){
            $nom=$user->nom;
            $prenom=$user->prenom;
            $filename=$nom."_".$prenom."-".$filename;
        }
        $path=$this->generateFilePath($id,$mimetype);
        header("Content-Type: $mimetype");
        header("Content-Disposition: inline; filename=$filename");
        readfile($path);
        die(200);
    }


    public function removeDocument($id_document)
    {
        $ret=["status"=>false,"message"=>null];

        $path=$this->generateFilePath($id_document,"application/pdf");
        if(unlink($path)===true){
            $this->delete($id_document);
            $ret["status"]=true;
        }

        return $ret;
    }
}