<?php

namespace App\Models\Vues;

require_once 'config/database.php';

abstract class Vue
{
    private static $bdd;

    protected string $table;

    public static function getInstance()
    {
        if (self::$bdd == null) {
            /* Attempt to connect to MySQL database */
            try {
                self::$bdd = new \PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME . ';charset=utf8', DB_USERNAME,
                    DB_PASSWORD);
            } catch (\Exception $e) {
                throw new \Exception("Error: " . $e->getMessage());
            }
            //self::$bdd->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
            self::$bdd->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        }
        return self::$bdd;
    }

    public function alreadyExists($key,$value){
        $db = self::getInstance();

        $query = 'SELECT count(*) FROM ' . $this->table. ' WHERE '.$key.' = :key;';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':key',$value);
        $stmt->execute();
        $result = $stmt->fetchColumn();
        return ["status"=>$result>0];
    }

    public function getAll(): array
    {
        $db = self::getInstance();
        $query = "SELECT * FROM $this->table";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return $result;
    }

    /**
     * recherche par un seul paramètre
     * @param string $param
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    public function findByParam(string $param,$value):array{
        $db = self::getInstance();

        $query = 'SELECT * FROM ' . $this->table. ' WHERE '.$param.' = :p;';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':p',$value);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return ["status"=>!empty($result),"data"=>empty($result)?null:$result];
    }

    function findByMultipleParams(array $params,array $values):array{
        $ret=["status"=>false,"data"=>null,"message"=>null];
        $paramsSize=sizeof($params);
        $valuesSize=sizeof($values);
        if($paramsSize!==$valuesSize){
            $ret["message"]="number of values and params are not the same";
            return $ret;
        }
        if($paramsSize<1){
            $ret["message"]="at least one param must be given";
            return $ret;
        }
        $db = self::getInstance();

        $wherePart="WHERE $params[0] = :$params[0]";
        for($i=1;$i<$paramsSize;$i++){
            $wherePart.=" AND $params[$i] = :$params[$i]";
        }
        $query = 'SELECT * FROM ' . $this->table.' '.$wherePart.";";


        $stmt = $db->prepare($query);
        for($j=0;$j<$paramsSize;$j++){
            $stmt->bindParam(":$params[$j]",$values[$j]);
        }
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        return ["status"=>!empty($result),"data"=>empty($result)?null:$result];
    }

}