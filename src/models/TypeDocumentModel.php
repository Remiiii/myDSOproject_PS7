<?php

namespace App\Models;

require_once 'models/Model.php';
class TypeDocumentModel extends Model
{
    protected string $table="type_document";
    protected string $primary_key="id_type_document";
    protected bool $auto_increment=true;

    protected array $attributes=[
        "id_type_document"=>null, //primary key,
        "libelle"=>"required"
    ];

}