<?php

namespace App\Models;

require_once 'Model.php';

use App\Models\Model;

class SalleModel extends Model
{
    protected string $table = 'salle';
    protected string $primary_key = "id_salle";
    protected bool $auto_increment = true;

    protected array $attributes = [
        'id_salle'=>null,
        'nom' => "required",
        'date_deb' => "required",
        'date_fin' => "required"
    ];
}
?>
