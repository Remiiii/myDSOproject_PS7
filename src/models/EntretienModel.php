<?php

namespace App\Models;

require_once 'Model.php';

use App\Models\Model;

class EntretienModel extends Model
{
    protected string $table = 'entretien';
    protected string $primary_key = "id_entretien";
    protected bool $auto_increment = true;

    protected array $attributes = [
        'id_entretien' => null,
        'id_planning' => "required",
        'id_salle' => "required",
        'date_deb' => "required",
        'date_fin' => "required",
        'confidentiel' => "required"
    ];
}