-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : mar. 16 nov. 2021 à 09:11
-- Version du serveur : 10.6.4-MariaDB-1:10.6.4+maria~focal
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `stage_database`
--

-- --------------------------------------------------------

--
-- Structure de la table `assiste`
--

CREATE TABLE `assiste` (
  `id_utilisateur` int(11) UNSIGNED NOT NULL,
  `id_entretien` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assiste`
--

INSERT INTO `assiste` (`id_utilisateur`, `id_entretien`) VALUES
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `id_document` int(11) UNSIGNED NOT NULL,
  `id_utilisateur` int(11) UNSIGNED NOT NULL,
  `id_type_document` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `document`
--

INSERT INTO `document` (`id_document`, `id_utilisateur`, `id_type_document`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `entretien`
--

CREATE TABLE `entretien` (
  `id_entretien` int(11) UNSIGNED NOT NULL,
  `id_planning` int(11) UNSIGNED NOT NULL,
  `id_salle` int(11) UNSIGNED NOT NULL,
  `id_utilisateur` int(11) UNSIGNED NOT NULL COMMENT 'Etudiant',
  `date_deb` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `confidentiel` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `entretien`
--

INSERT INTO `entretien` (`id_entretien`, `id_planning`, `id_salle`, `id_utilisateur`, `date_deb`, `date_fin`, `confidentiel`) VALUES
(1, 2, 1, 7, '2021-11-01 08:00:00', '2021-11-01 08:45:00', 0),
(2, 2, 2, 5, '2021-11-01 08:00:00', '2021-11-01 08:45:00', 0),
(3, 2, 2, 6, '2021-11-02 09:30:00', '2021-11-02 10:15:00', 0),
(4, 2, 1, 6, '2021-11-01 08:45:00', '2021-11-01 09:30:00', 0),
(5, 2, 2, 5, '2021-11-02 11:00:00', '2021-11-02 11:45:00', 0),
(6, 2, 2, 5, '2021-11-01 11:45:00', '2021-11-01 12:30:00', 0),
(7, 2, 1, 7, '2021-11-01 11:45:00', '2021-11-01 12:30:00', 0),
(8, 2, 2, 5, '2021-11-01 11:00:00', '2021-11-01 11:45:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `jury`
--

CREATE TABLE `jury` (
  `id_jury` int(11) UNSIGNED NOT NULL,
  `id_planning` int(11) UNSIGNED NOT NULL,
  `id_salle` int(11) UNSIGNED NOT NULL,
  `id_utilisateur1` int(11) UNSIGNED NOT NULL,
  `id_utilisateur2` int(11) UNSIGNED NOT NULL,
  `date_deb` datetime NOT NULL,
  `date_fin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `jury`
--

INSERT INTO `jury` (`id_jury`, `id_planning`, `id_salle`, `id_utilisateur1`, `id_utilisateur2`, `date_deb`, `date_fin`) VALUES
(1, 2, 1, 2, 3, '2021-11-01 00:00:00', '2021-11-03 00:00:00'),
(2, 2, 2, 8, 1, '2021-11-01 00:00:00', '2021-11-02 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

CREATE TABLE `planning` (
  `id_planning` int(10) UNSIGNED NOT NULL,
  `id_type_planning` int(11) UNSIGNED NOT NULL,
  `valide` tinyint(1) NOT NULL,
  `date_deb` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `heure_deb` time NOT NULL,
  `heure_fin` time NOT NULL,
  `duree_creneau` int(11) UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `planning`
--

INSERT INTO `planning` (`id_planning`, `id_type_planning`, `valide`, `date_deb`, `date_fin`, `heure_deb`, `heure_fin`, `duree_creneau`, `nom`) VALUES
(1, 1, 0, '2021-11-01 00:00:00', '2021-11-30 00:00:00', '08:30:00', '18:30:00', 30, 'Planning démo 1'),
(2, 1, 0, '2021-11-01 00:00:00', '2021-11-03 00:00:00', '08:00:00', '18:00:00', 45, 'Planning démo 2');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `id_salle` int(10) UNSIGNED NOT NULL,
  `nom` varchar(65) NOT NULL,
  `date_deb` datetime NOT NULL,
  `date_fin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id_salle`, `nom`, `date_deb`, `date_fin`) VALUES
(1, 'A104', '2021-11-01 00:00:00', '2021-12-31 00:00:00'),
(2, 'D005', '2021-11-01 00:00:00', '2021-11-02 19:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `type_document`
--

CREATE TABLE `type_document` (
  `id_type_document` int(11) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_document`
--

INSERT INTO `type_document` (`id_type_document`, `libelle`) VALUES
(1, 'CV'),
(2, 'Lettre de motivation'),
(3, 'Lettre de recommandation'),
(4, 'Offre de stage');

-- --------------------------------------------------------

--
-- Structure de la table `type_planning`
--

CREATE TABLE `type_planning` (
  `id_type_planning` int(11) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_planning`
--

INSERT INTO `type_planning` (`id_type_planning`, `libelle`) VALUES
(1, 'Soutenance'),
(2, 'Entretien');

-- --------------------------------------------------------

--
-- Structure de la table `type_utilisateur`
--

CREATE TABLE `type_utilisateur` (
  `id_type_utilisateur` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `etudiant` tinyint(1) NOT NULL,
  `besoin_validation` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_utilisateur`
--

INSERT INTO `type_utilisateur` (`id_type_utilisateur`, `libelle`, `etudiant`, `besoin_validation`) VALUES
(1, 'Administrateur', 0, 1),
(2, 'Professeur', 0, 1),
(3, 'Entreprise', 0, 1),
(4, 'Exterieur', 0, 0),
(5, 'CyberLog4', 1, 1),
(6, 'CyberLog5', 1, 1),
(7, 'CyberData', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_utilisateur` int(10) UNSIGNED NOT NULL,
  `id_type_utilisateur` int(11) UNSIGNED DEFAULT NULL,
  `nom` varchar(65) NOT NULL,
  `prenom` varchar(65) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `mot_de_passe` varchar(128) NOT NULL,
  `valide` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `id_type_utilisateur`, `nom`, `prenom`, `mail`, `mot_de_passe`, `valide`) VALUES
(1, 1, 'Administrateur', 'Administrateur', 'admin@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(2, 2, 'Professeur', 'Professeur', 'Professeur@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(3, 3, 'Entreprise', 'Entreprise', 'Entreprise@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(4, 4, 'Exterieur', 'Exterieur', 'Exterieur@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 0),
(5, 5, 'CyberLog4', 'CyberLog4', 'CyberLog4@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(6, 6, 'CyberLog5', 'CyberLog5', 'CyberLog5@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(7, 7, 'CyberData', 'CyberData', 'CyberData@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1),
(8, 2, 'prof2', 'prof2', 'prof2@test.fr', '6b0761934a6459b1c807dbf7fa2a5436c165b69f183b5ed557f3532b9302537e90c41f8840e1eb3e259b895207cc04dea4eed656aa7fe2c70cdf0e82906ca520', 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_assistent`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_assistent` (
`id_planning` int(10) unsigned
,`nom_planning` varchar(50)
,`id_utilisateur` int(11) unsigned
,`nom_utilisateur` varchar(65)
,`prenom_utilisateur` varchar(65)
,`id_entretien` int(11) unsigned
,`id_etudiant_presentation` int(11) unsigned
,`nom_etudiant_presentation` varchar(65)
,`prenom_etudiant_presentation` varchar(65)
,`id_salle` int(11) unsigned
,`nom_salle` varchar(65)
,`date_deb` datetime
,`date_fin` datetime
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_documents`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_documents` (
`id_document` int(11) unsigned
,`id_utilisateur` int(11) unsigned
,`nom_utilisateur` varchar(65)
,`prenom_utilisateur` varchar(65)
,`id_type_utilisateur` int(10) unsigned
,`type_utilisateur` varchar(50)
,`etudiant` tinyint(1)
,`id_type_document` int(11) unsigned
,`type_document` varchar(50)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_entretiens`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_entretiens` (
`id_entretien` int(11) unsigned
,`date_deb` datetime
,`date_fin` datetime
,`duree_entretien` int(11) unsigned
,`confidentiel` tinyint(1)
,`id_planning` int(10) unsigned
,`nom_planninng` varchar(50)
,`id_salle` int(10) unsigned
,`nom_salle` varchar(65)
,`id_etudiant` int(10) unsigned
,`nom_etudiant` varchar(65)
,`prenom_etudiant` varchar(65)
,`id_jury` int(11) unsigned
,`id_membre_jury_1` int(11) unsigned
,`nom_membre_jury_1` varchar(65)
,`prenom_membre_jury_1` varchar(65)
,`id_membre_jury_2` int(11) unsigned
,`nom_membre_jury_2` varchar(65)
,`prenom_membre_jury_2` varchar(65)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_etudiants`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_etudiants` (
`id_utilisateur` int(10) unsigned
,`id_type_utilisateur` int(11) unsigned
,`filiere` varchar(50)
,`nom` varchar(65)
,`prenom` varchar(65)
,`mail` varchar(255)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_jurys`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_jurys` (
`id_jury` int(11) unsigned
,`id_planning` int(11) unsigned
,`id_salle` int(11) unsigned
,`nom_salle` varchar(65)
,`id_utilisateur1` int(11) unsigned
,`nom_utilisateur1` varchar(65)
,`prenom_utilisateur1` varchar(65)
,`id_utilisateur2` int(11) unsigned
,`nom_utilisateur2` varchar(65)
,`prenom_utilisateur2` varchar(65)
,`date_deb` datetime
,`date_fin` datetime
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_plannings`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_plannings` (
`id_planning` int(10) unsigned
,`id_type_planning` int(11) unsigned
,`type_planning` varchar(50)
,`valide` tinyint(1)
,`date_deb` datetime
,`date_fin` datetime
,`heure_deb` time
,`heure_fin` time
,`duree_creneau` int(11) unsigned
,`nom` varchar(50)
);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `vue_utilisateurs`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `vue_utilisateurs` (
`id_utilisateur` int(10) unsigned
,`id_type_utilisateur` int(11) unsigned
,`type_utilisateur` varchar(50)
,`etudiant` tinyint(1)
,`nom` varchar(65)
,`prenom` varchar(65)
,`mail` varchar(255)
,`valide` tinyint(1)
);

-- --------------------------------------------------------

--
-- Structure de la vue `vue_assistent`
--
DROP TABLE IF EXISTS `vue_assistent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_assistent`  AS SELECT `planning`.`id_planning` AS `id_planning`, `planning`.`nom` AS `nom_planning`, `assiste`.`id_utilisateur` AS `id_utilisateur`, `utilisateur`.`nom` AS `nom_utilisateur`, `utilisateur`.`prenom` AS `prenom_utilisateur`, `assiste`.`id_entretien` AS `id_entretien`, `entretien`.`id_utilisateur` AS `id_etudiant_presentation`, (select `utilisateur`.`nom` from `utilisateur` where `utilisateur`.`id_utilisateur` = `entretien`.`id_utilisateur`) AS `nom_etudiant_presentation`, (select `utilisateur`.`prenom` from `utilisateur` where `utilisateur`.`id_utilisateur` = `entretien`.`id_utilisateur`) AS `prenom_etudiant_presentation`, `entretien`.`id_salle` AS `id_salle`, `salle`.`nom` AS `nom_salle`, `entretien`.`date_deb` AS `date_deb`, `entretien`.`date_fin` AS `date_fin` FROM ((((`assiste` join `salle`) join `utilisateur`) join `entretien`) join `planning`) WHERE `assiste`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `assiste`.`id_entretien` = `entretien`.`id_entretien` AND `entretien`.`id_salle` = `salle`.`id_salle` AND `entretien`.`id_planning` = `planning`.`id_planning` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_documents`
--
DROP TABLE IF EXISTS `vue_documents`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_documents`  AS SELECT `document`.`id_document` AS `id_document`, `document`.`id_utilisateur` AS `id_utilisateur`, `utilisateur`.`nom` AS `nom_utilisateur`, `utilisateur`.`prenom` AS `prenom_utilisateur`, `type_utilisateur`.`id_type_utilisateur` AS `id_type_utilisateur`, `type_utilisateur`.`libelle` AS `type_utilisateur`, `type_utilisateur`.`etudiant` AS `etudiant`, `document`.`id_type_document` AS `id_type_document`, `type_document`.`libelle` AS `type_document` FROM (((`document` join `type_document`) join `utilisateur`) join `type_utilisateur`) WHERE `document`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `document`.`id_type_document` = `type_document`.`id_type_document` AND `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id_type_utilisateur` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_entretiens`
--
DROP TABLE IF EXISTS `vue_entretiens`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_entretiens`  AS SELECT `entretien`.`id_entretien` AS `id_entretien`, `entretien`.`date_deb` AS `date_deb`, `entretien`.`date_fin` AS `date_fin`, `planning`.`duree_creneau` AS `duree_entretien`, `entretien`.`confidentiel` AS `confidentiel`, `planning`.`id_planning` AS `id_planning`, `planning`.`nom` AS `nom_planninng`, `salle`.`id_salle` AS `id_salle`, `salle`.`nom` AS `nom_salle`, `utilisateur`.`id_utilisateur` AS `id_etudiant`, `utilisateur`.`nom` AS `nom_etudiant`, `utilisateur`.`prenom` AS `prenom_etudiant`, `jury`.`id_jury` AS `id_jury`, `jury`.`id_utilisateur1` AS `id_membre_jury_1`, (select `utilisateur`.`nom` from `utilisateur` where `jury`.`id_utilisateur1` = `utilisateur`.`id_utilisateur`) AS `nom_membre_jury_1`, (select `utilisateur`.`prenom` from `utilisateur` where `jury`.`id_utilisateur1` = `utilisateur`.`id_utilisateur`) AS `prenom_membre_jury_1`, `jury`.`id_utilisateur2` AS `id_membre_jury_2`, (select `utilisateur`.`nom` from `utilisateur` where `jury`.`id_utilisateur2` = `utilisateur`.`id_utilisateur`) AS `nom_membre_jury_2`, (select `utilisateur`.`prenom` from `utilisateur` where `jury`.`id_utilisateur2` = `utilisateur`.`id_utilisateur`) AS `prenom_membre_jury_2` FROM ((((`entretien` join `salle`) join `utilisateur`) join `planning`) join `jury`) WHERE `entretien`.`id_planning` = `planning`.`id_planning` AND `entretien`.`id_utilisateur` = `utilisateur`.`id_utilisateur` AND `entretien`.`id_salle` = `salle`.`id_salle` AND `salle`.`id_salle` = `jury`.`id_salle` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_etudiants`
--
DROP TABLE IF EXISTS `vue_etudiants`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_etudiants`  AS SELECT `utilisateur`.`id_utilisateur` AS `id_utilisateur`, `utilisateur`.`id_type_utilisateur` AS `id_type_utilisateur`, `type_utilisateur`.`libelle` AS `filiere`, `utilisateur`.`nom` AS `nom`, `utilisateur`.`prenom` AS `prenom`, `utilisateur`.`mail` AS `mail` FROM (`utilisateur` join `type_utilisateur`) WHERE `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id_type_utilisateur` AND `type_utilisateur`.`etudiant` = 1 ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_jurys`
--
DROP TABLE IF EXISTS `vue_jurys`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_jurys`  AS SELECT `jury`.`id_jury` AS `id_jury`, `jury`.`id_planning` AS `id_planning`, `jury`.`id_salle` AS `id_salle`, `salle`.`nom` AS `nom_salle`, `jury`.`id_utilisateur1` AS `id_utilisateur1`, (select `utilisateur`.`nom` from `utilisateur` where `jury`.`id_utilisateur1` = `utilisateur`.`id_utilisateur`) AS `nom_utilisateur1`, (select `utilisateur`.`prenom` from `utilisateur` where `jury`.`id_utilisateur1` = `utilisateur`.`id_utilisateur`) AS `prenom_utilisateur1`, `jury`.`id_utilisateur2` AS `id_utilisateur2`, (select `utilisateur`.`nom` from `utilisateur` where `jury`.`id_utilisateur2` = `utilisateur`.`id_utilisateur`) AS `nom_utilisateur2`, (select `utilisateur`.`prenom` from `utilisateur` where `jury`.`id_utilisateur2` = `utilisateur`.`id_utilisateur`) AS `prenom_utilisateur2`, `jury`.`date_deb` AS `date_deb`, `jury`.`date_fin` AS `date_fin` FROM (`jury` join `salle`) WHERE `jury`.`id_salle` = `salle`.`id_salle` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_plannings`
--
DROP TABLE IF EXISTS `vue_plannings`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_plannings`  AS SELECT `planning`.`id_planning` AS `id_planning`, `planning`.`id_type_planning` AS `id_type_planning`, `type_planning`.`libelle` AS `type_planning`, `planning`.`valide` AS `valide`, `planning`.`date_deb` AS `date_deb`, `planning`.`date_fin` AS `date_fin`, `planning`.`heure_deb` AS `heure_deb`, `planning`.`heure_fin` AS `heure_fin`, `planning`.`duree_creneau` AS `duree_creneau`, `planning`.`nom` AS `nom` FROM (`planning` join `type_planning`) WHERE `planning`.`id_type_planning` = `type_planning`.`id_type_planning` ;

-- --------------------------------------------------------

--
-- Structure de la vue `vue_utilisateurs`
--
DROP TABLE IF EXISTS `vue_utilisateurs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vue_utilisateurs`  AS SELECT `utilisateur`.`id_utilisateur` AS `id_utilisateur`, `utilisateur`.`id_type_utilisateur` AS `id_type_utilisateur`, `type_utilisateur`.`libelle` AS `type_utilisateur`, `type_utilisateur`.`etudiant` AS `etudiant`, `utilisateur`.`nom` AS `nom`, `utilisateur`.`prenom` AS `prenom`, `utilisateur`.`mail` AS `mail`, `utilisateur`.`valide` AS `valide` FROM (`utilisateur` join `type_utilisateur`) WHERE `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id_type_utilisateur` ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `assiste`
--
ALTER TABLE `assiste`
  ADD PRIMARY KEY (`id_utilisateur`,`id_entretien`),
  ADD KEY `id_utilisateur` (`id_utilisateur`),
  ADD KEY `id_entretien` (`id_entretien`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id_document`),
  ADD KEY `id_utilisateur` (`id_utilisateur`),
  ADD KEY `type_document` (`id_type_document`);

--
-- Index pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD PRIMARY KEY (`id_entretien`),
  ADD KEY `id_planning` (`id_planning`),
  ADD KEY `id_salle` (`id_salle`),
  ADD KEY `id_utilisateur` (`id_utilisateur`);

--
-- Index pour la table `jury`
--
ALTER TABLE `jury`
  ADD PRIMARY KEY (`id_jury`),
  ADD KEY `id_salle` (`id_salle`),
  ADD KEY `id_utilisateur1` (`id_utilisateur1`),
  ADD KEY `id_utilisateur2` (`id_utilisateur2`),
  ADD KEY `id_planning` (`id_planning`);

--
-- Index pour la table `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`id_planning`),
  ADD KEY `id_type_planning` (`id_type_planning`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`id_salle`);

--
-- Index pour la table `type_document`
--
ALTER TABLE `type_document`
  ADD PRIMARY KEY (`id_type_document`);

--
-- Index pour la table `type_planning`
--
ALTER TABLE `type_planning`
  ADD PRIMARY KEY (`id_type_planning`);

--
-- Index pour la table `type_utilisateur`
--
ALTER TABLE `type_utilisateur`
  ADD PRIMARY KEY (`id_type_utilisateur`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`),
  ADD UNIQUE KEY `unique_email` (`mail`),
  ADD KEY `id_type_utilisateur` (`id_type_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
  MODIFY `id_document` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `entretien`
--
ALTER TABLE `entretien`
  MODIFY `id_entretien` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `jury`
--
ALTER TABLE `jury`
  MODIFY `id_jury` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `planning`
--
ALTER TABLE `planning`
  MODIFY `id_planning` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `id_salle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `type_document`
--
ALTER TABLE `type_document`
  MODIFY `id_type_document` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `type_planning`
--
ALTER TABLE `type_planning`
  MODIFY `id_type_planning` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `type_utilisateur`
--
ALTER TABLE `type_utilisateur`
  MODIFY `id_type_utilisateur` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_utilisateur` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `assiste`
--
ALTER TABLE `assiste`
  ADD CONSTRAINT `assiste_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `assiste_ibfk_2` FOREIGN KEY (`id_entretien`) REFERENCES `entretien` (`id_entretien`) ON DELETE CASCADE;

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`id_type_document`) REFERENCES `type_document` (`id_type_document`) ON DELETE CASCADE;

--
-- Contraintes pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD CONSTRAINT `entretien_ibfk_1` FOREIGN KEY (`id_planning`) REFERENCES `planning` (`id_planning`) ON DELETE CASCADE,
  ADD CONSTRAINT `entretien_ibfk_2` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`) ON DELETE CASCADE,
  ADD CONSTRAINT `entretien_ibfk_3` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `jury`
--
ALTER TABLE `jury`
  ADD CONSTRAINT `jury_ibfk_1` FOREIGN KEY (`id_utilisateur1`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `jury_ibfk_2` FOREIGN KEY (`id_utilisateur2`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `jury_ibfk_3` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`) ON DELETE CASCADE,
  ADD CONSTRAINT `jury_ibfk_4` FOREIGN KEY (`id_planning`) REFERENCES `planning` (`id_planning`);

--
-- Contraintes pour la table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `planning_ibfk_1` FOREIGN KEY (`id_type_planning`) REFERENCES `type_planning` (`id_type_planning`) ON DELETE CASCADE;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`id_type_utilisateur`) REFERENCES `type_utilisateur` (`id_type_utilisateur`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
